package com.projektseminar.dioxinfo.contactmanagement;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

public class ContactManager {
    private static final String TAG = "ContactManager";

    public static void OpenEmailClient(Activity activity, String subject, String mail, String name, String message){
        try{
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto:"));
            intent.putExtra(Intent.EXTRA_EMAIL, new String[] { mail });
            intent.putExtra(Intent.EXTRA_SUBJECT, subject);
            intent.putExtra(Intent.EXTRA_TEXT, message + '\n' + '\n' + name);
            activity.startActivity(Intent.createChooser(intent, ""));
        }
        catch(Exception exc){
            Log.v(TAG, exc.toString());
        }
    }



}
