package com.projektseminar.dioxinfo.screens.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;

import com.projektseminar.dioxinfo.contentmanagement.data.UserLanguage;
import com.projektseminar.dioxinfo.contentmanagement.data.UserType;
import com.projektseminar.dioxinfo.screens.activities.ActivityMain;

import java.util.Locale;

public class UserSettingsManager {

    private static final String TAG = "UserSettingsManager";
    private static UserSettingsManager userSettingsManager;

    private UserType userType;
    private UserLanguage userLanguage;
    private boolean darkMode;

    private UserSettingsManager() { }

    public static UserSettingsManager getInstance(){
        if (userSettingsManager == null) {
            userSettingsManager = new UserSettingsManager();
        }
        return userSettingsManager;
    }

    public void loadFromSharedPref(Context context) {
        SharedPreferences sp = context.getSharedPreferences("preferences", Context.MODE_PRIVATE);
        int userTypeInt = sp.getInt("Type", -1);
        int userLanguageInt = sp.getInt("Language", -1);
        boolean darkModeBool = sp.getBoolean("Darkmode", false);
        if (userType == null) {
            if (userTypeInt != -1) {
                try {
                    userSettingsManager.userType = UserType.values()[userTypeInt];
                } catch (Exception e) {
                    Log.e(TAG, "Could not parse UserType from Shared Preferences to the appropriate Enum");
                }
            }
        }
        if (userLanguage == null) {
            if (userLanguageInt != -1) {
                try {
                    userSettingsManager.userLanguage = UserLanguage.values()[userLanguageInt];
                }
                catch (Exception e) {
                    Log.e(TAG, "Could not parse UserLanguage from Shared Preferences to the appropriate Enum");
                }
            }
        }
        if (!darkMode) {
            darkMode = darkModeBool;
            ActivityMainUiManager.getInstance().updateUiMode();
        }
    }

    public void saveToSharedPref(Context context) {
        SharedPreferences sp = context.getSharedPreferences("preferences", Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = sp.edit();
        ed.putInt("Type", userType.ordinal());
        ed.putInt("Language", userLanguage.ordinal());
        ed.putBoolean("Darkmode", darkMode);
        ed.apply();
    }

    public UserType getUserType(){
        return userType;
    }

    public UserLanguage getUserLanguage(){
        return userLanguage;
    }

    public boolean isDarkModeEnabled() {
        return darkMode;
    }

    public void setDarkMode(boolean darkMode) {
        this.darkMode = darkMode;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public void setUserLanguage(UserLanguage userLanguage) {
        this.userLanguage = userLanguage;
    }

    public void setAppLocale(ActivityMain activity) {
        String localeCode = userSettingsManager.getUserLanguage().toString();
        if (localeCode != null) {
            Resources res = activity.getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                conf.setLocale(new Locale(localeCode.toLowerCase()));
            }
            else {
                conf.locale = new Locale(localeCode.toLowerCase());
            }
            res.updateConfiguration(conf, dm);
        }
    }
}
