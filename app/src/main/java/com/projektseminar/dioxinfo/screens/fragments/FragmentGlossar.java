package com.projektseminar.dioxinfo.screens.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.projektseminar.dioxinfo.R;
import com.projektseminar.dioxinfo.adapter.GlossaryListAdapter;
import com.projektseminar.dioxinfo.contentmanagement.data.Glossary;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FragmentGlossar extends Fragment {

    public static final String TAG = "Fragment_Glossar";

    private RecyclerView recyclerView;
    private List<Button> buttonList;
    private GlossaryListAdapter listAdapter;

    public static FragmentGlossar newInstance(){
        return new FragmentGlossar();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ArrayList <Glossary> generatedGlossaryList = new ArrayList<>();
        ArrayList <String> entryList = new ArrayList<>();
        entryList.add(getString(R.string.entry1));
        entryList.add(getString(R.string.entry2));
        entryList.add(getString(R.string.entry3));
        entryList.add(getString(R.string.entry4));
        entryList.add(getString(R.string.entry5));
        entryList.add(getString(R.string.entry6));
        entryList.add(getString(R.string.entry7));
        entryList.add(getString(R.string.entry8));
        entryList.add(getString(R.string.entry9));
        entryList.add(getString(R.string.entry10));
        entryList.add(getString(R.string.entry11));
        entryList.add(getString(R.string.entry12));
        entryList.add(getString(R.string.entry13));
        entryList.add(getString(R.string.entry14));
        entryList.add(getString(R.string.entry15));
        entryList.add(getString(R.string.entry16));
        entryList.add(getString(R.string.entry17));
        entryList.add(getString(R.string.entry18));
        entryList.add(getString(R.string.entry19));
        entryList.add(getString(R.string.entry20));
        entryList.add(getString(R.string.entry21));
        entryList.add(getString(R.string.entry22));
        entryList.add(getString(R.string.entry23));
        entryList.add(getString(R.string.entry24));
        entryList.add(getString(R.string.entry25));

        //sort array-list with string data alphabetically
        Collections.sort(entryList);
        //iteration through all resources of the list
        for(String entry : entryList){
            Glossary currentEntry = createGlossaryEntryFromString(entry);
            //only if glossary "word" is not empty
            if (!currentEntry.getWord().equals("")){
                generatedGlossaryList.add(currentEntry);
            }
        }

        listAdapter = new GlossaryListAdapter(generatedGlossaryList);
        View view = inflater.inflate(R.layout.fragment_glossary, container, false);
        recyclerView = view.findViewById(R.id.recycler_view_glossary);
        recyclerView.setAdapter(listAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        return view;
    }

    public Glossary createGlossaryEntryFromString(String entry){
        try {
            String[] entryArray = entry.split("%%§%%");
            return new Glossary(entryArray[0], entryArray[1]+"\n");
        }
        catch(ArrayIndexOutOfBoundsException e) {
            return new Glossary("", "");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        buttonList = new ArrayList<>();
        buttonList.add((Button)view.findViewById(R.id.btn_a));
        buttonList.add((Button)view.findViewById(R.id.btn_b));
        buttonList.add((Button)view.findViewById(R.id.btn_c));
        buttonList.add((Button)view.findViewById(R.id.btn_d));
        buttonList.add((Button)view.findViewById(R.id.btn_e));
        buttonList.add((Button)view.findViewById(R.id.btn_f));
        buttonList.add((Button)view.findViewById(R.id.btn_g));
        buttonList.add((Button)view.findViewById(R.id.btn_h));
        buttonList.add((Button)view.findViewById(R.id.btn_i));
        buttonList.add((Button)view.findViewById(R.id.btn_j));
        buttonList.add((Button)view.findViewById(R.id.btn_k));
        buttonList.add((Button)view.findViewById(R.id.btn_l));
        buttonList.add((Button)view.findViewById(R.id.btn_m));
        buttonList.add((Button)view.findViewById(R.id.btn_n));
        buttonList.add((Button)view.findViewById(R.id.btn_o));
        buttonList.add((Button)view.findViewById(R.id.btn_p));
        buttonList.add((Button)view.findViewById(R.id.btn_q));
        buttonList.add((Button)view.findViewById(R.id.btn_r));
        buttonList.add((Button)view.findViewById(R.id.btn_s));
        buttonList.add((Button)view.findViewById(R.id.btn_t));
        buttonList.add((Button)view.findViewById(R.id.btn_u));
        buttonList.add((Button)view.findViewById(R.id.btn_v));
        buttonList.add((Button)view.findViewById(R.id.btn_w));
        buttonList.add((Button)view.findViewById(R.id.btn_x));
        buttonList.add((Button)view.findViewById(R.id.btn_y));
        buttonList.add((Button)view.findViewById(R.id.btn_z));

        for (final Button button : buttonList) {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    jumpToLetter(button.getText());
                }
            });
        }
    }

    private void jumpToLetter(CharSequence letter){
        LinearLayoutManager llm = (LinearLayoutManager) recyclerView.getLayoutManager();
        ArrayList<Glossary> list = listAdapter.getGlossaryList();
        int position = 0;
        for (Glossary word : list) {
            if (word.getWord().toLowerCase().substring(0, 1).equals(letter.toString().toLowerCase())) {
                position = list.indexOf(word);
                break;
            }
        }
        if (llm != null) {
            llm.scrollToPositionWithOffset(position, 0);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
