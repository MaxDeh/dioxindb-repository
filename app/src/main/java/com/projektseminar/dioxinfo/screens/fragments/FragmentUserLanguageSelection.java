package com.projektseminar.dioxinfo.screens.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.projektseminar.dioxinfo.R;
import com.projektseminar.dioxinfo.contentmanagement.data.UserLanguage;
import com.projektseminar.dioxinfo.screens.managers.CustomFragmentManager;
import com.projektseminar.dioxinfo.screens.managers.UserSettingsManager;

public class FragmentUserLanguageSelection extends Fragment {

    public static final String TAG = "FragmentUserLanguageSelection";

    public FragmentUserLanguageSelection(){ }

    public static FragmentUserLanguageSelection newInstance(){
        return new FragmentUserLanguageSelection();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_language_selection, container, false);
        view.findViewById(R.id.german).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUserLanguageDestroyFragment(UserLanguage.de);
            }
        });
        view.findViewById(R.id.english).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUserLanguageDestroyFragment(UserLanguage.en);
            }
        });
        return view;
    }

    private void setUserLanguageDestroyFragment(UserLanguage userLanguage) {
        UserSettingsManager.getInstance().setUserLanguage(userLanguage);
        CustomFragmentManager.getInstance().removeFragment(TAG);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
