package com.projektseminar.dioxinfo.screens.managers;

import android.app.Activity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.projektseminar.dioxinfo.R;
import com.projektseminar.dioxinfo.contentmanagement.data.UserType;
import com.projektseminar.dioxinfo.screens.activities.ActivityMain;
import com.projektseminar.dioxinfo.screens.fragments.FragmentAboutUs;
import com.projektseminar.dioxinfo.screens.fragments.FragmentContact;
import com.projektseminar.dioxinfo.screens.fragments.FragmentGlossar;
import com.projektseminar.dioxinfo.screens.fragments.FragmentNewsticker;
import com.projektseminar.dioxinfo.screens.fragments.FragmentPublications;
import com.projektseminar.dioxinfo.screens.fragments.FragmentSearchResults;
import com.projektseminar.dioxinfo.screens.fragments.FragmentUserSettings;
import com.projektseminar.dioxinfo.screens.fragments.FragmentWissenswertes;
import com.projektseminar.dioxinfo.screens.fragments.FragmentWissenswertesBasis;

import java.lang.ref.WeakReference;

public class ActivityMainOnClickManager {

    private static ActivityMainOnClickManager onClickManager;

    private WeakReference<ActivityMain> activity;

    //top action bar
    private WeakReference<ImageButton> btnMenue;
    private WeakReference<ImageButton> btnSearch;
    private WeakReference<EditText> etSearch;
    private WeakReference<TextView> tvTitle;
    //bottom action bar
    private WeakReference<ImageButton> btnLeft;
    private WeakReference<ImageButton> btnMiddle;
    private WeakReference<ImageButton> btnRight;
    //dropdown menu
    private WeakReference<LinearLayout> layoutInfo;
    private WeakReference<LinearLayout> layoutGlossary;
    private WeakReference<LinearLayout> layoutPublications;
    private WeakReference<LinearLayout> layoutContact;
    private WeakReference<LinearLayout> layoutSettings;
    private WeakReference<LinearLayout> layoutAboutApp;

    //OnClickListeners
    private View.OnClickListener menu;
    private View.OnClickListener backpress;
    private View.OnClickListener newsticker;
    private View.OnClickListener publications;
    private View.OnClickListener info;
    private View.OnClickListener info_content_biota;
    private View.OnClickListener info_content_luft;
    private View.OnClickListener info_content_boden;
    private View.OnClickListener info_content_mensch;
    private View.OnClickListener info_content_proben;
    private View.OnClickListener info_content_massnahmen;
    private View.OnClickListener info_content_toxizitaet;
    private View.OnClickListener info_content_wirkprinzipien;
    private View.OnClickListener search;
    private TextView.OnEditorActionListener startSearch;
    private View.OnClickListener glossary;
    private View.OnClickListener contact;
    private View.OnClickListener settings;
    private View.OnClickListener about;

    private ActivityMainOnClickManager(){ }

    public static ActivityMainOnClickManager getInstance() {
        if (ActivityMainOnClickManager.onClickManager == null) {
            ActivityMainOnClickManager.onClickManager = new ActivityMainOnClickManager();
        }
        return ActivityMainOnClickManager.onClickManager;
    }

    public void setActivity(ActivityMain activity) {
        onClickManager.activity = new WeakReference<>(activity);
    }

    public void referenceElements(){
        //top action bar
        onClickManager.btnMenue = new WeakReference<>((ImageButton) activity.get().findViewById(R.id.btn_menue));
        onClickManager.btnSearch = new WeakReference<>((ImageButton) activity.get().findViewById(R.id.btn_search));
        onClickManager.etSearch = new WeakReference<>((EditText) activity.get().findViewById(R.id.edit_text_searchbar));
        onClickManager.tvTitle = new WeakReference<>((TextView) activity.get().findViewById(R.id.page_title_textview));
        //bottom action bar
        onClickManager.btnLeft = new WeakReference<>((ImageButton) activity.get().findViewById(R.id.btn_left));
        onClickManager.btnMiddle = new WeakReference<>((ImageButton) activity.get().findViewById(R.id.btn_middle));
        onClickManager.btnRight = new WeakReference<>((ImageButton) activity.get().findViewById(R.id.btn_right));
        //dropdown menu
        onClickManager.layoutInfo = new WeakReference<>((LinearLayout) activity.get().findViewById(R.id.layout_info));
        onClickManager.layoutGlossary = new WeakReference<>((LinearLayout) activity.get().findViewById(R.id.layout_glossary));
        onClickManager.layoutPublications = new WeakReference<>((LinearLayout) activity.get().findViewById(R.id.layout_publications));
        onClickManager.layoutContact = new WeakReference<>((LinearLayout) activity.get().findViewById(R.id.layout_contact));
        onClickManager.layoutSettings = new WeakReference<>((LinearLayout) activity.get().findViewById(R.id.layout_settings));
        onClickManager.layoutAboutApp = new WeakReference<>((LinearLayout) activity.get().findViewById(R.id.layout_about_app));
    }

    public void initializeOnClickListeners(){
        onClickManager.menu = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityMainUiManager.getInstance().toggleMenuVisibility();
            }
        };

        onClickManager.backpress = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.get().onBackPressed();
            }
        };

        onClickManager.newsticker = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //CustomFragmentManager.getInstance().addFragment(R.id.fragment_placeholder, FragmentNewsticker.newInstance(), FragmentNewsticker.TAG);
                CustomFragmentManager.getInstance().killAllFragments();
            }
        };

        onClickManager.search = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityMainUiManager uim = ActivityMainUiManager.getInstance();
                if (!uim.isSearchModeActive()) {
                    uim.enterSearchMode();
                }
            }
        };

        onClickManager.startSearch = new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    EditText et = onClickManager.etSearch.get();
                    TextView tvTitle = onClickManager.tvTitle.get();
                    InputMethodManager imm = (InputMethodManager) activity.get().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(et.getWindowToken(), 0);
                    String searchString = etSearch.get().getText().toString();
                    if (!searchString.equals("")) {
                        et.setVisibility(View.INVISIBLE);
                        tvTitle.setVisibility(View.VISIBLE);
                        CustomFragmentManager.getInstance().addFragment(R.id.fragment_placeholder, FragmentSearchResults.newInstance(searchString), FragmentSearchResults.TAG);
                        et.setText("", TextView.BufferType.EDITABLE);
                    }
                    return true;
                }
                return false;
            }
        };

        onClickManager.publications = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomFragmentManager.getInstance().addFragment(R.id.fragment_placeholder, FragmentPublications.newInstance(), FragmentPublications.TAG);
            }
        };
        onClickManager.info = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomFragmentManager.getInstance().addFragment(R.id.fragment_placeholder, FragmentWissenswertes.newInstance(), FragmentWissenswertes.TAG);
            }
        };

        onClickManager.info_content_biota = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomFragmentManager.getInstance().addFragment(R.id.fragment_placeholder,
                        FragmentWissenswertesBasis.newInstance(FragmentWissenswertesBasis.InfoContent.BIOTA), "Fragment_Wissenswertes_Basis");
            }
        };
        onClickManager.info_content_luft = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomFragmentManager.getInstance().addFragment(R.id.fragment_placeholder,
                        FragmentWissenswertesBasis.newInstance(FragmentWissenswertesBasis.InfoContent.LUFT), "Fragment_Wissenswertes_Basis");
            }
        };
        onClickManager.info_content_mensch = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomFragmentManager.getInstance().addFragment(R.id.fragment_placeholder,
                        FragmentWissenswertesBasis.newInstance(FragmentWissenswertesBasis.InfoContent.MENSCH), "Fragment_Wissenswertes_Basis");
            }
        };
        onClickManager.info_content_boden = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomFragmentManager.getInstance().addFragment(R.id.fragment_placeholder,
                        FragmentWissenswertesBasis.newInstance(FragmentWissenswertesBasis.InfoContent.BODEN), "Fragment_Wissenswertes_Basis");
            }
        };
        onClickManager.info_content_proben = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomFragmentManager.getInstance().addFragment(R.id.fragment_placeholder,
                        FragmentWissenswertesBasis.newInstance(FragmentWissenswertesBasis.InfoContent.PROBEN), "Fragment_Wissenswertes_Basis");
            }
        };
        onClickManager.info_content_massnahmen = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomFragmentManager.getInstance().addFragment(R.id.fragment_placeholder,
                        FragmentWissenswertesBasis.newInstance(FragmentWissenswertesBasis.InfoContent.MASSNAHMEN), "Fragment_Wissenswertes_Basis");
            }
        };
        onClickManager.info_content_toxizitaet = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomFragmentManager.getInstance().addFragment(R.id.fragment_placeholder,
                        FragmentWissenswertesBasis.newInstance(FragmentWissenswertesBasis.InfoContent.TOXIZITAET), "Fragment_Wissenswertes_Basis");
            }
        };
        onClickManager.info_content_wirkprinzipien = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomFragmentManager.getInstance().addFragment(R.id.fragment_placeholder,
                        FragmentWissenswertesBasis.newInstance(FragmentWissenswertesBasis.InfoContent.WIRKPRINZIPIEN), "Fragment_Wissenswertes_Basis");
            }
        };

        onClickManager.glossary = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomFragmentManager.getInstance().addFragment(R.id.fragment_placeholder, FragmentGlossar.newInstance(), FragmentGlossar.TAG);
            }
        };
        onClickManager.contact = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomFragmentManager.getInstance().addFragment(R.id.fragment_placeholder, FragmentContact.newInstance(), FragmentContact.TAG);
            }
        };
        onClickManager.settings = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomFragmentManager.getInstance().addFragment(R.id.fragment_placeholder, FragmentUserSettings.newInstance(), FragmentUserSettings.TAG);
            }
        };
        onClickManager.about = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomFragmentManager.getInstance().addFragment(R.id.fragment_placeholder, FragmentAboutUs.newInstance(), FragmentAboutUs.TAG);
            }
        };
    }

    public void swapTopMenuSubpage() {
        btnMenue.get().setOnClickListener(backpress);
    }

    public void swapTopMenuMainpage() {
        btnMenue.get().setOnClickListener(menu);
    }

    public void setNewOnClickListeners(UserType userType) {
        setUniversalOnClickListeners();
        switch (userType) {
            case CITIZEN:
                btnLeft.get().setOnClickListener(glossary);
                btnRight.get().setOnClickListener(newsticker);
                break;
            case PRESS:
                btnLeft.get().setOnClickListener(glossary);
                btnMiddle.get().setOnClickListener(newsticker);
                btnRight.get().setOnClickListener(info);
                break;
            case SCIENTIST:
                btnLeft.get().setOnClickListener(publications);
                btnMiddle.get().setOnClickListener(newsticker);
                btnRight.get().setOnClickListener(info);
                break;
        }
    }

    private void setUniversalOnClickListeners() {
        btnMenue.get().setOnClickListener(menu);
        layoutInfo.get().setOnClickListener(info);
        layoutAboutApp.get().setOnClickListener(about);
        layoutContact.get().setOnClickListener(contact);
        layoutGlossary.get().setOnClickListener(glossary);
        layoutPublications.get().setOnClickListener(publications);
        layoutSettings.get().setOnClickListener(settings);
        btnSearch.get().setOnClickListener(search);
        etSearch.get().setOnEditorActionListener(startSearch);
    }

    public void setOnClicksForWissenswertes(FrameLayout flBiota, FrameLayout flMensch, FrameLayout flBoden, FrameLayout flLuft,
                                            FrameLayout flProben,FrameLayout flMassnahmen, FrameLayout flWirkprinzipien,
                                            FrameLayout flToxizitaet ) {
        flBiota.setOnClickListener(info_content_biota);
        flMensch.setOnClickListener(info_content_mensch);
        flBoden.setOnClickListener(info_content_boden);
        flLuft.setOnClickListener(info_content_luft);
        flProben.setOnClickListener(info_content_proben);
        flMassnahmen.setOnClickListener(info_content_massnahmen);
        flWirkprinzipien.setOnClickListener(info_content_wirkprinzipien);
        flToxizitaet.setOnClickListener(info_content_toxizitaet);
    }

    public void onPostFragmentChanged() {
        String top = "";
        try {
            top = activity.get().getSupportFragmentManager().getBackStackEntryAt(activity.get().getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        switch (top) {
            case FragmentNewsticker.TAG:
                swapTopMenuMainpage();
                break;
            case FragmentGlossar.TAG:
            case FragmentUserSettings.TAG:
            case FragmentWissenswertes.TAG:
            case FragmentWissenswertesBasis.TAG:
            case FragmentPublications.TAG:
            case FragmentContact.TAG:
            case FragmentSearchResults.TAG:
            case FragmentAboutUs.TAG:
                swapTopMenuSubpage();
                break;
        }
    }
}
