package com.projektseminar.dioxinfo.screens.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.projektseminar.dioxinfo.R;
import com.projektseminar.dioxinfo.contentmanagement.data.UserType;
import com.projektseminar.dioxinfo.screens.managers.ActivityMainUiManager;
import com.projektseminar.dioxinfo.screens.managers.UserSettingsManager;

public class FragmentUserTypeSelection extends Fragment {

    public static final String TAG = "FragmentUserTypeSelection";

    public FragmentUserTypeSelection(){ }

    public static FragmentUserTypeSelection newInstance(){
        return  new FragmentUserTypeSelection();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_type_selection, container, false);
        view.findViewById(R.id.citizen).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUserTypeDestroyFragment(UserType.CITIZEN);
            }
        });
        view.findViewById(R.id.press).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUserTypeDestroyFragment(UserType.PRESS);
            }
        });
        view.findViewById(R.id.scientist).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUserTypeDestroyFragment(UserType.SCIENTIST);
            }
        });
        return view;
    }

    private void setUserTypeDestroyFragment(UserType userType){
        UserSettingsManager.getInstance().setUserType(userType);
        ActivityMainUiManager.getInstance().recreateActivity(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
