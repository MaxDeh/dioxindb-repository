package com.projektseminar.dioxinfo.screens.managers;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.projektseminar.dioxinfo.R;
import com.projektseminar.dioxinfo.screens.activities.ActivityMain;
import com.projektseminar.dioxinfo.screens.fragments.FragmentNewsticker;
import com.projektseminar.dioxinfo.screens.fragments.FragmentSearchResults;

import java.lang.ref.WeakReference;

public class CustomFragmentManager {

    public static final String TAG = "CustomFragmentManager";

    private static CustomFragmentManager cfm;
    private WeakReference<ActivityMain> activity;
    private boolean allowReloadFlag;

    private CustomFragmentManager(){ }

    public static CustomFragmentManager getInstance() {
        if (cfm == null) {
            cfm = new CustomFragmentManager();
        }
        return cfm;
    }

    public void setActivity(ActivityMain activity) {
        cfm.activity = new WeakReference<>(activity);
    }

    public void setAllowReloadFlag(boolean allowReloadFlag) {
        this.allowReloadFlag = allowReloadFlag;
    }

    public void addFragment(int containerViewId, Fragment fragment, @NonNull String fragmentTag) {
        FragmentManager fm = activity.get().getSupportFragmentManager();
        FragmentTransaction ft = cfm.activity.get().getSupportFragmentManager().beginTransaction();

        String topBackStackEntry = "";

        //grab the top backstack name
        try {
            topBackStackEntry = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1).getName();
        }
        catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }

        //check if the fragment is a duplicate - if so, do not add anything!
        if (topBackStackEntry != null) {
            if (fragmentTag.equals(topBackStackEntry) && !(fragment instanceof FragmentSearchResults)) {
                return;
            }
        }

        if (!(fragment instanceof FragmentNewsticker)) {
            ft.setCustomAnimations(R.anim.enter_from_bottom, R.anim.exit_to_bottom, R.anim.enter_from_bottom, R.anim.exit_to_bottom);
        }
        ft.add(containerViewId, fragment, fragmentTag);
        ft.addToBackStack(fragmentTag);
        ft.commit();
        fm.executePendingTransactions();
        ActivityMainUiManager.getInstance().onPostFragmentChanged();
        ActivityMainOnClickManager.getInstance().onPostFragmentChanged();
    }

    public void removeFragment(String fragmentTag) {
        FragmentManager fm = activity.get().getSupportFragmentManager();
        FragmentTransaction ft = cfm.activity.get().getSupportFragmentManager().beginTransaction();
        if (fm.findFragmentByTag(fragmentTag) != null) {
            ft.remove(fm.findFragmentByTag(fragmentTag));
        }
        ft.commit();
        fm.executePendingTransactions();
        fm.popBackStackImmediate();
    }

    public void killAllFragments(){
        FragmentManager fm = activity.get().getSupportFragmentManager();
        for (int i = fm.getBackStackEntryCount() - 1; i > 0; i--) {
            fm.popBackStack();
        }
        fm.executePendingTransactions();
        ActivityMainUiManager.getInstance().onPostFragmentChanged();
        ActivityMainOnClickManager.getInstance().onPostFragmentChanged();
    }

    public void reloadNewsFragment(){
        if (allowReloadFlag) {
            killAllFragments();
            FragmentManager fm = activity.get().getSupportFragmentManager();
            FragmentTransaction ft = cfm.activity.get().getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_placeholder, FragmentNewsticker.newInstance(), FragmentNewsticker.TAG);
            ft.commit();
            fm.executePendingTransactions();
            allowReloadFlag = false;
        }
    }
}

