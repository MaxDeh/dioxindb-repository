package com.projektseminar.dioxinfo.screens.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.projektseminar.dioxinfo.R;
import com.projektseminar.dioxinfo.contactmanagement.ContactManager;

public class FragmentContact extends Fragment {

    public static final String TAG = "FragmentContact";

    private EditText subject, name, message;

    public FragmentContact(){ }

    public static FragmentContact newInstance(){
        return new FragmentContact();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contact, container, false);
        subject = view.findViewById(R.id.et_subject);
        name = view.findViewById(R.id.et_name);
        message = view.findViewById(R.id.et_message);
        view.findViewById(R.id.btn_send).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ContactManager.OpenEmailClient(getActivity(), subject.getText().toString(),
                                                                      "admin@mailadresse.uba.de",
                                                                      name.getText().toString(),
                                                                      message.getText().toString());
                    }
                }
        );
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
