package com.projektseminar.dioxinfo.screens.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.projektseminar.dioxinfo.R;
import com.projektseminar.dioxinfo.adapter.SearchResultsListAdapter;
import com.projektseminar.dioxinfo.contentmanagement.SearchManager;

public class FragmentSearchResults extends Fragment {

    public static final String TAG = "FragmentSearchResults";

    private RecyclerView recyclerView;
    private String searchString;

    public static FragmentSearchResults newInstance(String searchString){
        FragmentSearchResults fragment = new FragmentSearchResults();
        fragment.searchString = searchString;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        SearchResultsListAdapter listAdapter = new SearchResultsListAdapter(SearchManager.getInstance().generateAllSearchResults(searchString), this.getContext());
        View view = inflater.inflate(R.layout.fragment_search_results, container, false);
        if (listAdapter.getContentList() == null || (listAdapter.getContentList().size()) == 0) {
            view.findViewById(R.id.no_results_found).setVisibility(View.VISIBLE);
        }
        recyclerView = view.findViewById(R.id.recycler_view_results);
        recyclerView.setAdapter(listAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
