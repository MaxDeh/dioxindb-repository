package com.projektseminar.dioxinfo.screens.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.projektseminar.dioxinfo.R;
import com.projektseminar.dioxinfo.adapter.PublicationsListAdapter;

public class FragmentPublications extends Fragment {

    public static final String TAG = "FragmentPublications";

    private RecyclerView recyclerView;

    public static FragmentPublications newInstance(){
        FragmentPublications fragment = new FragmentPublications();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        PublicationsListAdapter listAdapter = new PublicationsListAdapter(getContext());
        View view = inflater.inflate(R.layout.fragment_publications, container, false);
        recyclerView = view.findViewById(R.id.recycler_view_publications);
        recyclerView.setAdapter(listAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
