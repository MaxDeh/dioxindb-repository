package com.projektseminar.dioxinfo.screens.fragments;

import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.projektseminar.dioxinfo.R;

public class FragmentWissenswertesBasis extends Fragment {

    private InfoContent infoContent;
    private TextView site_title;
    private TextView link1;
    private TextView link2;
    private TextView link3;
    private TextView link4;
    private TextView link5;


    private TextView textbox1;
    private TextView textbox2;
    private TextView textbox3;
    private TextView textbox4;
    private TextView textbox5;
    private TextView textbox6;
    private TextView textbox7;

    private TextView title1;
    private TextView title2;
    private TextView title3;
    private TextView title4;
    private TextView title5;
    private TextView title6;
    private TextView title7;

    private TextView title_small1;
    private TextView title_small2;
    private TextView title_small3;
    private TextView title_small4;
    private TextView title_small5;
    private TextView title_small6;
    private TextView title_small7;


    public static final String TAG = "Fragment_Wissenswertes_Basis";

    public static FragmentWissenswertesBasis newInstance(InfoContent infoContent){
        int i = Integer.valueOf(android.os.Build.VERSION.SDK);
        FragmentWissenswertesBasis fragment = new FragmentWissenswertesBasis();
        fragment.infoContent = infoContent;
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wissenswertes_basis, container, false);

        textbox1 = view.findViewById(R.id.tv_wissenswertes_textbox_1);
        textbox2 = view.findViewById(R.id.tv_wissenswertes_textbox_2);
        textbox3 = view.findViewById(R.id.tv_wissenswertes_textbox_3);
        textbox4 = view.findViewById(R.id.tv_wissenswertes_textbox_4);
        textbox5 = view.findViewById(R.id.tv_wissenswertes_textbox_5);
        textbox6 = view.findViewById(R.id.tv_wissenswertes_textbox_6);
        textbox7 = view.findViewById(R.id.tv_wissenswertes_textbox_7);

        site_title = view.findViewById(R.id.tv_wissenswertes_unterseite_titel);
        link1 = view.findViewById(R.id.tv_link1);
        link2 = view.findViewById(R.id.tv_link2);
        link3 = view.findViewById(R.id.tv_link3);
        link4 = view.findViewById(R.id.tv_link4);
        link5 = view.findViewById(R.id.tv_link5);

        title1 = view.findViewById(R.id.tv_info_title_1);
        title2 = view.findViewById(R.id.tv_info_title_2);
        title3 = view.findViewById(R.id.tv_info_title_3);
        title4 = view.findViewById(R.id.tv_info_title_4);
        title5 = view.findViewById(R.id.tv_info_title_5);
        title6 = view.findViewById(R.id.tv_info_title_6);
        title7 = view.findViewById(R.id.tv_info_title_7);

        title_small1 = view.findViewById(R.id.tv_info_title_klein_1);
        title_small2 = view.findViewById(R.id.tv_info_title_klein_2);
        title_small3 = view.findViewById(R.id.tv_info_title_klein_3);
        title_small4 = view.findViewById(R.id.tv_info_title_klein_4);
        title_small5 = view.findViewById(R.id.tv_info_title_klein_5);
        title_small6 = view.findViewById(R.id.tv_info_title_klein_6);
        title_small7 = view.findViewById(R.id.tv_info_title_klein_7);

        //hier je nach Unterseite mit Inhalt befüllen
        switch (this.infoContent){
            case BIOTA:
                setTextinTextview(site_title,R.string.biota);
                setTextinTextview(textbox1, R.string.biota_textbox1);
                break;
            case LUFT:
                setTextinTextview(site_title,R.string.air);
                setTextinTextview(textbox1, R.string.luft_textbox1);
                setTextinTextview(title2, R.string.luft_title2);
                setTextinTextview(textbox2, R.string.luft_textbox2);
                setTextinTextview(title3, R.string.luft_title3);
                setTextinTextview(textbox3, R.string.luft_textbox3);
                setTextinTextview(textbox4, R.string.luft_textbox4);
                setTextinTextview(title5, R.string.luft_title5);
                setTextinTextview(textbox5, R.string.luft_textbox5);
                break;
            case MENSCH:
                setTextinTextview(site_title,R.string.human);
                setTextinTextview(textbox1, R.string.mensch_textbox1);
                setTextinTextview(textbox2, R.string.mensch_textbox2);
                setTextinTextview(textbox3, R.string.mensch_textbox3);
                setTextinTextview(textbox4, R.string.mensch_textbox4);
                setTextinTextview(textbox5, R.string.mensch_textbox5);
                setLinkinTextview(link1, R.string.mensch_link1);
                break;
            case BODEN:
                setTextinTextview(site_title,R.string.earth);
                setTextinTextview(textbox1, R.string.boden_textbox1);
                setTextinTextview(title2, R.string.boden_title2);
                setTextinTextview(textbox2, R.string.boden_textbox2);
                setTextinTextview(title3, R.string.boden_title3);
                setTextinTextview(textbox3, R.string.boden_textbox3);
                setTextinTextview(title4, R.string.boden_title4);
                setTextinTextview(textbox4, R.string.boden_textbox4);
                setTextinTextview(title5, R.string.boden_title5);
                setTextinTextview(textbox5, R.string.boden_textbox5);
                setTextinTextview(title6, R.string.boden_title6);
                setTextinTextview(textbox6, R.string.boden_textbox6);
                setTextinTextview(title7, R.string.boden_title7);
                setTextinTextview(textbox7, R.string.boden_textbox7);
                setLinkinTextview(link1, R.string.boden_link1);
                setLinkinTextview(link2, R.string.boden_link2);
                break;
            case PROBEN:
                setTextinTextview(site_title,R.string.samples);
                setTextinTextview(textbox1, R.string.proben_textbox1);
                setTextinTextview(textbox2, R.string.proben_textbox2);
                setLinkinTextview(link1, R.string.proben_link1);
                break;
            case MASSNAHMEN:
                setTextinTextview(site_title,R.string.action);
                setTextinTextview(textbox1, R.string.massnahmen_textbox1);
                setTextinTextview(textbox2, R.string.massnahmen_textbox2);
                setTextinTextview(textbox3, R.string.massnahmen_textbox3);
                setTextinTextview(title4, R.string.massnahmen_title4);

                //set properties for the links
                setLinkinTextview(link1, R.string.massnahmen_link_1);
                setLinkinTextview(link2, R.string.massnahmen_link_2);
                setLinkinTextview(link3, R.string.massnahmen_link_3);
                setLinkinTextview(link4, R.string.massnahmen_link_4);
                setLinkinTextview(link5, R.string.massnahmen_link_5);
                break;
            case TOXIZITAET:
                setTextinTextview(site_title,R.string.poison);
                setTextinTextview(title1, R.string.toxizitaet_title1);
                setTextinTextview(title_small1, R.string.toxizitaet_title_small1);
                setTextinTextview(textbox1, R.string.toxizitaet_textbox1);
                setTextinTextview(textbox2, R.string.toxizitaet_textbox2);
                setTextinTextview(title3, R.string.toxizitaet_title3);
                setTextinTextview(textbox3, R.string.toxizitaet_textbox3);
                setTextinTextview(title_small4, R.string.toxizitaet_title_small4);
                setTextinTextview(textbox4, R.string.toxizitaet_textbox4);
                setTextinTextview(textbox5, R.string.toxizitaet_textbox5);
                setTextinTextview(title_small6, R.string.toxizitaet_title_small6);
                setTextinTextview(textbox6, R.string.toxizitaet_textbox6);
                break;
            case WIRKPRINZIPIEN:
                setTextinTextview(site_title, R.string.processes);
                setTextinTextview(title1, R.string.wirkprinzipien_title1);
                setTextinTextview(title_small1, R.string.wirkprinzipien_title_small1);
                setTextinTextview(textbox1, R.string.wirkprinzipien_textbox1);
                setTextinTextview(textbox2, R.string.wirkprinzipien_textbox2);
                setTextinTextview(title_small3, R.string.wirkprinzipien_title_small3);
                setTextinTextview(textbox3, R.string.wirkprinzipien_textbox3);
                setTextinTextview(title4, R.string.wirkprinzipien_title4);
                setTextinTextview(textbox4, R.string.wirkprinzipien_textbox4);
                setLinkinTextview(link1,R.string.wirkprinzipien_link_1);
                setLinkinTextview(link2, R.string.wirkprinzipien_link_2);
                break;
        }
        return view;
    }

    public void setTextinTextview(TextView textview, int text_resource){
        String string_resource = "";
        string_resource = getString(text_resource);
        textview.setText(string_resource);

        //turn visibility from gone to visible, if there is a text resource in selected language
        if(!string_resource.equals("")){
            textview.setVisibility(View.VISIBLE);
        }

    }
    public void setLinkinTextview(TextView textview, int text_resource){
        CharSequence string_resource ="";
        string_resource = getText(text_resource); //hyperlinks only working with "getText" instead of getString
        textview.setText(string_resource);
        textview.setPadding(0,5,0,15); //expand the padding bottom from 5 to 15, for more space to click on link
        textview.setMovementMethod(LinkMovementMethod.getInstance()); //make hyperlink clickable

        //turn visibility from gone to visible, if there is a text resource in selected language
        if(!string_resource.equals("")){
            textview.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public enum InfoContent {
        PROBEN,
        MASSNAHMEN,
        TOXIZITAET,
        WIRKPRINZIPIEN,
        BIOTA,
        BODEN,
        MENSCH,
        LUFT,
    }
}
