package com.projektseminar.dioxinfo.screens.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.projektseminar.dioxinfo.adapter.NewstickerListAdapter;
import com.projektseminar.dioxinfo.R;

public class FragmentNewsticker extends Fragment {

    public static final String TAG = "FragmentNewsticker";

    private RecyclerView recyclerView;

    public static FragmentNewsticker newInstance(){
        FragmentNewsticker fragment = new FragmentNewsticker();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        NewstickerListAdapter listAdapter = new NewstickerListAdapter();
        View view = inflater.inflate(R.layout.fragment_newsticker, container, false);
        recyclerView = view.findViewById(R.id.recycler_view_articles);
        recyclerView.setAdapter(listAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
