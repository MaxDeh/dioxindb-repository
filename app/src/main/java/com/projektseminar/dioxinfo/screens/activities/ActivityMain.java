package com.projektseminar.dioxinfo.screens.activities;

import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;

import com.projektseminar.dioxinfo.R;
import com.projektseminar.dioxinfo.contentmanagement.ArticleManager;
import com.projektseminar.dioxinfo.contentmanagement.PublicationManager;
import com.projektseminar.dioxinfo.databasemanagement.DatabaseManager;
import com.projektseminar.dioxinfo.screens.fragments.FragmentNewsticker;
import com.projektseminar.dioxinfo.screens.fragments.FragmentUserLanguageSelection;
import com.projektseminar.dioxinfo.screens.fragments.FragmentUserTypeSelection;
import com.projektseminar.dioxinfo.screens.managers.ActivityMainOnClickManager;
import com.projektseminar.dioxinfo.screens.managers.ActivityMainUiManager;
import com.projektseminar.dioxinfo.screens.managers.CustomFragmentManager;
import com.projektseminar.dioxinfo.screens.managers.UserSettingsManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

public class ActivityMain extends AppCompatActivity {

    private String TAG = "ActivityMain";

    private ArticleManager am;
    private PublicationManager pm;
    private DatabaseManager dbm;
    private CustomFragmentManager cfm;
    private ActivityMainOnClickManager ocm;
    private UserSettingsManager usm;
    private ActivityMainUiManager uim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get instances of all needed singletons!
        am = ArticleManager.getInstance();
        pm = PublicationManager.getInstance();
        cfm = CustomFragmentManager.getInstance();
        uim = ActivityMainUiManager.getInstance();
        ocm = ActivityMainOnClickManager.getInstance();
        usm = UserSettingsManager.getInstance();

        am.deserializeArticleList(this);
        pm.deserializePublicationList(this);

        //AsyncTask to update lists
        if (uim.isFirstInitialize()) {
            dbm = new DatabaseManager(this);
            dbm.update();
        }

        cfm.setActivity(this);
        uim.setActivity(this);
        ocm.setActivity(this);

        uim.referenceElements();
        ocm.referenceElements();

        ocm.initializeOnClickListeners();

        usm.loadFromSharedPref(this);
        if (usm.getUserType() == null) {
            uim.hideUiForInitialSelection();
            cfm.addFragment(R.id.fragment_placeholder, FragmentUserTypeSelection.newInstance(), FragmentUserTypeSelection.TAG);
        }
        if (usm.getUserLanguage() == null) {
            uim.hideUiForInitialSelection();
            cfm.addFragment(R.id.fragment_placeholder, FragmentUserLanguageSelection.newInstance(), FragmentUserLanguageSelection.TAG);
        }
        else if (usm.getUserType() != null && usm.getUserLanguage() != null) {
            initializeEntryScreen();
        }
    }

    public void initializeEntryScreen(){
        uim.showUiAfterInitialSelection();

        uim.setNewUi(usm.getUserType());
        ocm.setNewOnClickListeners(usm.getUserType());

        cfm.addFragment(R.id.fragment_placeholder, FragmentNewsticker.newInstance(), FragmentNewsticker.TAG);

        UserSettingsManager.getInstance().setAppLocale(this);

        cfm.setAllowReloadFlag(true);

        if (uim.isFirstInitialize()) {
            uim.setFirstInitialize(false);
            recreate();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        boolean initDispatcher = false;
        Rect menuRect = new Rect();
        Rect etRect = new Rect();
        findViewById(R.id.included_dropdown_menu).getGlobalVisibleRect(menuRect);
        findViewById(R.id.edit_text_searchbar).getGlobalVisibleRect(etRect);
        if (!menuRect.contains((int) ev.getRawX(), (int) ev.getRawY())) {
            if (uim.isMenuVisible()) {
                uim.closeMenu();
                initDispatcher = true;
                if (uim.isSearchModeActive()) {
                    uim.endSearchMode();
                }
            }
        }
        if (!etRect.contains((int) ev.getRawX(), (int) ev.getRawY())) {
            if (uim.isSearchModeActive()) {
                uim.endSearchMode();
                initDispatcher = true;
            }
        }
        if (initDispatcher) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                }
            }, 300);
            return true;
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        int backStackEntryCount = fm.getBackStackEntryCount();
        String topBackStackEntryTag = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 1).getName();
        if (fm.findFragmentByTag(FragmentUserTypeSelection.TAG) != null || fm.findFragmentByTag(FragmentUserLanguageSelection.TAG) != null) {
            finish();
        }
        if (backStackEntryCount == 1 && topBackStackEntryTag.equals(FragmentNewsticker.TAG)) {
            finish();
        }
        else {
            fm.popBackStack();
        }
        fm.executePendingTransactions();
        uim.onPostFragmentChanged();
        ocm.onPostFragmentChanged();
    }

    @Override
    protected void onDestroy() {
        try {
            usm.saveToSharedPref(this);
        }
        catch (NullPointerException e) {
            Log.e(TAG, "User type and / or user language couldn't be saved to the shared preferences because the fields weren't set in the UserSettingsManager class.");
        }
        super.onDestroy();
    }
}