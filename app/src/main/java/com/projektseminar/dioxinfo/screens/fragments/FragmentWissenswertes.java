package com.projektseminar.dioxinfo.screens.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.projektseminar.dioxinfo.R;
import com.projektseminar.dioxinfo.screens.managers.ActivityMainOnClickManager;

import androidx.fragment.app.Fragment;

public class FragmentWissenswertes extends Fragment {

    public static final String TAG = "Fragment_Wissenswertes";

    private FrameLayout flBiota;
    private FrameLayout flLuft;
    private FrameLayout flMensch;
    private FrameLayout flBoden;
    private FrameLayout flProben;
    private FrameLayout flMassnahmen;
    private FrameLayout flToxizitaet;
    private FrameLayout flWirkprinzipien;
    private FrameLayout flUba;
    private FrameLayout flGeo;
    private FrameLayout flDB;

    public static FragmentWissenswertes newInstance(){
        FragmentWissenswertes fragment = new FragmentWissenswertes();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wissenswertes, container, false);
        this.flBiota = view.findViewById(R.id.fl_biota);
        this.flLuft = view.findViewById(R.id.fl_luft);
        this.flMensch = view.findViewById(R.id.fl_mensch);
        this.flBoden = view.findViewById(R.id.fl_boden);
        this.flProben = view.findViewById(R.id.fl_proben);
        this.flMassnahmen = view.findViewById(R.id.fl_massnahmen);
        this.flToxizitaet = view.findViewById(R.id.fl_toxizitaet);
        this.flWirkprinzipien = view.findViewById(R.id.fl_wirkprinzipien);
        this.flUba = view.findViewById(R.id.fl_uba);
        this.flGeo = view.findViewById(R.id.fl_geo_db);
        this.flDB = view.findViewById(R.id.fl_dioxin_db);

        flUba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://www.umweltbundesamt.de/"));
                startActivity(i);
            }
        });

        flGeo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://www.dioxindb.de/f_daten_gis.html"));
                startActivity(i);
            }
        });

        flDB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse("https://popdioxin.uba.de/DioxinWSClient/logon.do?language=de"));
                startActivity(i);
            }
        });

        ActivityMainOnClickManager.getInstance().setOnClicksForWissenswertes(flBiota, flMensch,
                flBoden, flLuft,flProben, flMassnahmen, flWirkprinzipien, flToxizitaet);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


}
