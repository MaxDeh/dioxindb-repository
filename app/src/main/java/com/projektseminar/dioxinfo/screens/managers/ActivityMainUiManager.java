package com.projektseminar.dioxinfo.screens.managers;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;

import com.projektseminar.dioxinfo.R;
import com.projektseminar.dioxinfo.contentmanagement.data.UserType;
import com.projektseminar.dioxinfo.screens.activities.ActivityMain;
import com.projektseminar.dioxinfo.screens.fragments.FragmentAboutUs;
import com.projektseminar.dioxinfo.screens.fragments.FragmentContact;
import com.projektseminar.dioxinfo.screens.fragments.FragmentGlossar;
import com.projektseminar.dioxinfo.screens.fragments.FragmentNewsticker;
import com.projektseminar.dioxinfo.screens.fragments.FragmentPublications;
import com.projektseminar.dioxinfo.screens.fragments.FragmentSearchResults;
import com.projektseminar.dioxinfo.screens.fragments.FragmentUserSettings;
import com.projektseminar.dioxinfo.screens.fragments.FragmentWissenswertes;
import com.projektseminar.dioxinfo.screens.fragments.FragmentWissenswertesBasis;

import java.lang.ref.WeakReference;

public class ActivityMainUiManager {
    public static String TAG = "ActivityMainUiManager";
    private static ActivityMainUiManager uiManager;

    private boolean firstInitialize = true;
    private boolean isMenuVisible = false;
    private boolean isSearchModeActive = false;

    private WeakReference<ActivityMain> activity;
    //top action bar
    private WeakReference<TextView> tvPageTitle;
    private WeakReference<ImageButton> btnMenue;
    private WeakReference<ImageButton> btnSearch;
    private WeakReference<EditText> etSearch;
    //bottom action bar
    private WeakReference<LinearLayout> layoutBottom;
    private WeakReference<ImageButton> btnLeft;
    private WeakReference<ImageButton> btnMiddle;
    private WeakReference<ImageButton> btnRight;
    //dropdown menu
    private WeakReference<LinearLayout> layoutMenue;
    private WeakReference<LinearLayout> layoutInfo;
    private WeakReference<LinearLayout> layoutDividerUnderInfo;
    private WeakReference<LinearLayout> layoutGlossary;
    private WeakReference<LinearLayout> layoutDividerUnderGlossary;
    private WeakReference<LinearLayout> layoutPublications;
    private WeakReference<LinearLayout> layoutDividerUnderPublications;
    private WeakReference<LinearLayout> layoutContact;
    private WeakReference<LinearLayout> layoutDividerUnderContact;
    private WeakReference<LinearLayout> layoutSettings;
    private WeakReference<LinearLayout> layoutDividerUnderSettings;
    private WeakReference<LinearLayout> layoutAboutApp;

    private ActivityMainUiManager() {}

    public static ActivityMainUiManager getInstance() {
        if (uiManager == null) {
            uiManager = new ActivityMainUiManager();
        }
        return uiManager;
    }

    public void setActivity(ActivityMain activity) {
        uiManager.activity = new WeakReference<>(activity);
    }

    public void recreateActivity(Fragment fragmentToDestroy) {
        if (fragmentToDestroy != null) {
            CustomFragmentManager.getInstance().removeFragment(fragmentToDestroy.getTag());
        }
        CustomFragmentManager.getInstance().killAllFragments();
        uiManager.activity.get().recreate();
        activity.get().initializeEntryScreen();
    }

    public boolean isFirstInitialize() {
        return firstInitialize;
    }

    public void setFirstInitialize(boolean firstInitialize) {
        this.firstInitialize = firstInitialize;
    }

    public boolean isMenuVisible() {
        return isMenuVisible;
    }

    public void referenceElements(){
        //top action bar
        uiManager.tvPageTitle = new WeakReference<>((TextView) activity.get().findViewById(R.id.page_title_textview));
        uiManager.btnMenue = new WeakReference<>((ImageButton) activity.get().findViewById(R.id.btn_menue));
        uiManager.btnSearch = new WeakReference<>((ImageButton) activity.get().findViewById(R.id.btn_search));
        uiManager.etSearch = new WeakReference<>((EditText) activity.get().findViewById(R.id.edit_text_searchbar));
        //bottom action bar
        uiManager.layoutBottom = new WeakReference<>((LinearLayout) activity.get().findViewById(R.id.included_bottom_action_bar));
        uiManager.btnLeft = new WeakReference<>((ImageButton) activity.get().findViewById(R.id.btn_left));
        uiManager.btnMiddle = new WeakReference<>((ImageButton) activity.get().findViewById(R.id.btn_middle));
        uiManager.btnRight = new WeakReference<>((ImageButton) activity.get().findViewById(R.id.btn_right));
        //dropdown menu and divider layouts
        uiManager.layoutMenue = new WeakReference<>((LinearLayout) activity.get().findViewById(R.id.included_dropdown_menu));
        uiManager.layoutInfo = new WeakReference<>((LinearLayout) activity.get().findViewById(R.id.layout_info));
        uiManager.layoutDividerUnderInfo = new WeakReference<>((LinearLayout) activity.get().findViewById(R.id.divider_under_info));
        uiManager.layoutGlossary = new WeakReference<>((LinearLayout) activity.get().findViewById(R.id.layout_glossary));
        uiManager.layoutDividerUnderGlossary = new WeakReference<>((LinearLayout) activity.get().findViewById(R.id.divider_under_glossary));
        uiManager.layoutPublications = new WeakReference<>((LinearLayout) activity.get().findViewById(R.id.layout_publications));
        uiManager.layoutDividerUnderPublications = new WeakReference<>((LinearLayout) activity.get().findViewById(R.id.divider_under_publications));
        uiManager.layoutContact = new WeakReference<>((LinearLayout) activity.get().findViewById(R.id.layout_contact));
        uiManager.layoutDividerUnderContact = new WeakReference<>((LinearLayout) activity.get().findViewById(R.id.divider_under_contact));
        uiManager.layoutSettings = new WeakReference<>((LinearLayout) activity.get().findViewById(R.id.layout_settings));
        uiManager.layoutDividerUnderSettings = new WeakReference<>((LinearLayout) activity.get().findViewById(R.id.divider_under_settings));
        uiManager.layoutAboutApp = new WeakReference<>((LinearLayout) activity.get().findViewById(R.id.layout_about_app));
    }

    public void hideUiForInitialSelection() {
        tvPageTitle.get().setText(R.string.welcome);
        layoutBottom.get().setVisibility(View.INVISIBLE);
        btnMenue.get().setVisibility(View.INVISIBLE);
        btnSearch.get().setVisibility(View.INVISIBLE);
        btnLeft.get().setVisibility(View.INVISIBLE);
        btnMiddle.get().setVisibility(View.INVISIBLE);
        btnRight.get().setVisibility(View.INVISIBLE);
    }

    public void showUiAfterInitialSelection(){
        layoutBottom.get().setVisibility(View.VISIBLE);
        btnMenue.get().setVisibility(View.VISIBLE);
        btnSearch.get().setVisibility(View.VISIBLE);
        btnLeft.get().setVisibility(View.VISIBLE);
        btnMiddle.get().setVisibility(View.VISIBLE);
        btnRight.get().setVisibility(View.VISIBLE);
    }

    private void resetVisibility(){
        uiManager.btnLeft.get().setVisibility(View.VISIBLE);
        uiManager.btnMiddle.get().setVisibility(View.VISIBLE);
        uiManager.btnRight.get().setVisibility(View.VISIBLE);
        uiManager.layoutInfo.get().setVisibility(View.VISIBLE);
        uiManager.layoutDividerUnderInfo.get().setVisibility(View.VISIBLE);
        uiManager.layoutGlossary.get().setVisibility(View.VISIBLE);
        uiManager.layoutDividerUnderGlossary.get().setVisibility(View.VISIBLE);
        uiManager.layoutPublications.get().setVisibility(View.VISIBLE);
        uiManager.layoutDividerUnderPublications.get().setVisibility(View.VISIBLE);
        uiManager.layoutContact.get().setVisibility(View.VISIBLE);
        uiManager.layoutDividerUnderContact.get().setVisibility(View.VISIBLE);
        uiManager.layoutSettings.get().setVisibility(View.VISIBLE);
        uiManager.layoutDividerUnderSettings.get().setVisibility(View.VISIBLE);
        uiManager.layoutAboutApp.get().setVisibility(View.VISIBLE);
    }

    public void setNewUi(UserType type){
        resetVisibility();
        switch (type) {
            case CITIZEN:
                btnMiddle.get().setVisibility(View.GONE);
                layoutGlossary.get().setVisibility(View.GONE);
                layoutDividerUnderGlossary.get().setVisibility(View.GONE);
                btnLeft.get().setImageDrawable(activity.get().getResources().getDrawable(R.drawable.glossary_24px));
                btnRight.get().setImageDrawable(activity.get().getResources().getDrawable(R.drawable.news_24px));
                break;
            case PRESS:
                layoutGlossary.get().setVisibility(View.GONE);
                layoutDividerUnderGlossary.get().setVisibility(View.GONE);
                layoutInfo.get().setVisibility(View.GONE);
                layoutDividerUnderInfo.get().setVisibility(View.GONE);
                btnLeft.get().setImageDrawable(activity.get().getResources().getDrawable(R.drawable.glossary_24px));
                btnMiddle.get().setImageDrawable(activity.get().getResources().getDrawable(R.drawable.news_24px));
                btnRight.get().setImageDrawable(activity.get().getResources().getDrawable(R.drawable.info_24px));
                break;
            case SCIENTIST:
                layoutInfo.get().setVisibility(View.GONE);
                layoutDividerUnderInfo.get().setVisibility(View.GONE);
                layoutPublications.get().setVisibility(View.GONE);
                layoutDividerUnderPublications.get().setVisibility(View.GONE);
                btnLeft.get().setImageDrawable(activity.get().getResources().getDrawable(R.drawable.publications_24px));
                btnMiddle.get().setImageDrawable(activity.get().getResources().getDrawable(R.drawable.news_24px));
                btnRight.get().setImageDrawable(activity.get().getResources().getDrawable(R.drawable.info_24px));
                break;
        }
    }

    public void updateUiMode() {
        if (UserSettingsManager.getInstance().isDarkModeEnabled()) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
        else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
    }

    public void toggleMenuVisibility() {
        View view = layoutMenue.get();
        if (layoutMenue.get().getVisibility()==View.INVISIBLE) {
            int width = view.getWidth();
            view.setTranslationX(-width);
            view.setVisibility(View.VISIBLE);
            view.animate()
                .translationX(0)
                .setDuration(300);
            isMenuVisible = true;
        }
        else {
            layoutMenue.get().setVisibility(View.INVISIBLE);
            isMenuVisible = false;
        }
    }

    public void closeMenu() {
        layoutMenue.get().setVisibility(View.INVISIBLE);
        isMenuVisible = false;
    }

    public void swapTopMenuSubpage() {
        btnMenue.get().setImageResource(R.drawable.arrow_back_24px_xml);
        btnSearch.get().setVisibility(View.INVISIBLE);
    }

    public void swapTopMenuMainpage() {
        btnMenue.get().setImageResource(R.drawable.menu_32px);
        btnSearch.get().setVisibility(View.VISIBLE);
    }

    public void onPostFragmentChanged() {
        String top;
        try {
            top = activity.get().getSupportFragmentManager().getBackStackEntryAt(activity.get().getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
        }
        catch (Exception e){
            top = "";
        }
        switch (top) {
            case FragmentNewsticker.TAG:
                tvPageTitle.get().setText(R.string.news);
                swapTopMenuMainpage();
                break;
            case FragmentGlossar.TAG:
                tvPageTitle.get().setText(R.string.glossar);
                swapTopMenuSubpage();
                break;
            case FragmentUserSettings.TAG:
                tvPageTitle.get().setText(R.string.settings);
                swapTopMenuSubpage();
                break;
            case FragmentWissenswertes.TAG:
            case FragmentWissenswertesBasis.TAG:
                tvPageTitle.get().setText(R.string.wissenswertes);
                swapTopMenuSubpage();
                break;
            case FragmentPublications.TAG:
                tvPageTitle.get().setText(R.string.publications);
                swapTopMenuSubpage();
                break;
            case FragmentContact.TAG:
                tvPageTitle.get().setText(R.string.contact);
                swapTopMenuSubpage();
                break;
            case FragmentSearchResults.TAG:
                tvPageTitle.get().setText(R.string.search_results);
                swapTopMenuSubpage();
                break;
            case FragmentAboutUs.TAG:
                tvPageTitle.get().setText(R.string.about);
                swapTopMenuSubpage();
                break;
        }
        if (uiManager.isMenuVisible) {
            uiManager.closeMenu();
        }
        if (uiManager.isSearchModeActive) {
            uiManager.endSearchMode();
        }
    }

    public boolean isSearchModeActive() {
        return isSearchModeActive;
    }

    public void endSearchMode() {
        InputMethodManager imm = (InputMethodManager) activity.get().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(etSearch.get().getWindowToken(), 0);
        etSearch.get().setVisibility(View.INVISIBLE);
        tvPageTitle.get().setVisibility(View.VISIBLE);
        etSearch.get().setText("", TextView.BufferType.EDITABLE);
        isSearchModeActive = false;
    }

    public void enterSearchMode() {
        tvPageTitle.get().setVisibility(View.INVISIBLE);
        etSearch.get().setVisibility(View.VISIBLE);
        etSearch.get().requestFocus();
        etSearch.get().callOnClick();
        InputMethodManager imm = (InputMethodManager) activity.get().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(etSearch.get(), InputMethodManager.SHOW_IMPLICIT);
        isSearchModeActive = true;
    }
}
