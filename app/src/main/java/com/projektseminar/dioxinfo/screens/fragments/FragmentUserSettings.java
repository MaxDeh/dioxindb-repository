package com.projektseminar.dioxinfo.screens.fragments;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.projektseminar.dioxinfo.R;
import com.projektseminar.dioxinfo.contentmanagement.data.UserLanguage;
import com.projektseminar.dioxinfo.contentmanagement.data.UserType;
import com.projektseminar.dioxinfo.screens.activities.ActivityMain;
import com.projektseminar.dioxinfo.screens.managers.ActivityMainUiManager;
import com.projektseminar.dioxinfo.screens.managers.UserSettingsManager;

public class FragmentUserSettings extends Fragment {

    public static final String TAG = "FragmentUserSettings";

    private TextView tvUserType, tvUserLanguage, tvEnableDarkMode;
    private Spinner spUserType, spUserLanguage;
    private CheckBox cbEnableDarkmode;
    private Button btnConfirm, btnCancel;
    private String citizen, press, scientist, de, en;

    public FragmentUserSettings(){ }

    public static FragmentUserSettings newInstance(){
        return new FragmentUserSettings();
    }

    private void initializeChoices() {
        UserSettingsManager usm = UserSettingsManager.getInstance();
        switch (usm.getUserType()) {
            case CITIZEN:
                spUserType.setSelection(0);
                break;
            case PRESS:
                spUserType.setSelection(1);
                break;
            case SCIENTIST:
                spUserType.setSelection(2);
                break;
        }
        switch (usm.getUserLanguage()) {
            case de:
                spUserLanguage.setSelection(0);
                break;
            case en:
                spUserLanguage.setSelection(1);
                break;
        }
        if(usm.isDarkModeEnabled()){
            cbEnableDarkmode.setChecked(true);
        }
        else {
            cbEnableDarkmode.setChecked(false);
        }
    }

    private void exitFragment(Boolean confirmSelection) {
        if (confirmSelection) {
            UserSettingsManager usm = UserSettingsManager.getInstance();
            switch (spUserType.getSelectedItemPosition()) {
                case 0:
                    usm.setUserType(UserType.CITIZEN);
                    break;
                case 1:
                    usm.setUserType(UserType.PRESS);
                    break;
                case 2:
                    usm.setUserType(UserType.SCIENTIST);
                    break;
            }
            switch (spUserLanguage.getSelectedItemPosition()) {
                case 0:
                    usm.setUserLanguage(UserLanguage.de);
                    break;
                case 1:
                    usm.setUserLanguage(UserLanguage.en);
                    break;
            }
            if(cbEnableDarkmode.isChecked()){
                usm.setDarkMode(true);
                ActivityMainUiManager.getInstance().updateUiMode();
            }
            else {
                usm.setDarkMode(false);
                ActivityMainUiManager.getInstance().updateUiMode();
            }

            ((ActivityMain)getActivity()).initializeEntryScreen();
            ActivityMainUiManager.getInstance().recreateActivity(this);
        }
        else {
            getActivity().onBackPressed();
        }
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_settings, container, false);
        tvUserType = view.findViewById(R.id.tv_user_type);
        tvUserLanguage = view.findViewById(R.id.tv_user_language);
        tvEnableDarkMode = view.findViewById(R.id.tv_darkmode_checkbox);
        spUserType = view.findViewById(R.id.sp_user_type);
        spUserLanguage = view.findViewById(R.id.sp_user_language);
        cbEnableDarkmode = view.findViewById(R.id.cb_darkmode);
        btnConfirm = view.findViewById(R.id.btn_confirm);
        btnCancel = view.findViewById(R.id.btn_cancel);

        Resources res = getResources();
        citizen = res.getString(R.string.citizen);
        press = res.getString(R.string.press);
        scientist = res.getString(R.string.scientist);
        de = res.getString(R.string.de);
        en = res.getString(R.string.en);
        String[] userTypeItems = new String[] { citizen, press, scientist };
        String[] languages = new String[] { de, en };

        ArrayAdapter<String> typeAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, userTypeItems);
        ArrayAdapter<String> languageAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, languages);
        spUserType.setAdapter(typeAdapter);
        spUserLanguage.setAdapter(languageAdapter);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exitFragment(false);
            }
        });
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exitFragment(true);
            }
        });

        initializeChoices();

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
