package com.projektseminar.dioxinfo.adapter;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.projektseminar.dioxinfo.R;
import com.projektseminar.dioxinfo.contentmanagement.data.Article;
import com.projektseminar.dioxinfo.contentmanagement.data.ContentWrapper;
import com.projektseminar.dioxinfo.contentmanagement.data.Publication;
import com.projektseminar.dioxinfo.contentmanagement.data.WebLink;

import java.util.ArrayList;

public class SearchResultsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final String TAG = "SearchResultsListAdapter";
    private ArrayList<ContentWrapper> contentList;
    private Context context;

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    public static class ViewHolderArticle extends RecyclerView.ViewHolder {
        private TextView tvTitle, tvText, tvDate, tvLink1, tvLink2, tvLink3, tvLink4, tvLink5;
        private ImageButton btnToggleSize;
        private boolean isElementExpanded;
        private ObjectAnimator animation;

        public ViewHolderArticle(View view) {
            super(view);
            // Define click listener for the ViewHolder's View
            tvTitle = view.findViewById(R.id.tv_title);
            tvText = view.findViewById(R.id.tv_text);
            tvDate = view.findViewById(R.id.tv_date);
            tvLink1 = view.findViewById(R.id.tv_link1);
            tvLink2 = view.findViewById(R.id.tv_link2);
            tvLink3 = view.findViewById(R.id.tv_link3);
            tvLink4 = view.findViewById(R.id.tv_link4);
            tvLink5 = view.findViewById(R.id.tv_link5);
            btnToggleSize = view.findViewById(R.id.btn_toggle_size);
            animation = new ObjectAnimator();
            isElementExpanded = false;
        }

        public TextView getTvDate() { return tvDate; }
        public TextView getTvTitle() {
            return tvTitle;
        }
        public TextView getTvText() {
            return tvText;
        }
        public TextView getTvLink1() { return tvLink1; }
        public TextView getTvLink2() { return tvLink2; }
        public TextView getTvLink3() { return tvLink3; }
        public TextView getTvLink4() { return tvLink4; }
        public TextView getTvLink5() { return tvLink5; }

        public ImageButton getBtnToggleSize() { return btnToggleSize; }

        public void toggleTextSize() {
            if (!isElementExpanded) {
                animation.cancel();
                animation = ObjectAnimator.ofInt(
                        tvText,
                        "maxLines",
                        150);
                animation.setDuration(1000);
                animation.start();
                btnToggleSize.setBackgroundResource(R.drawable.expand_less_24px);
                tvText.setEllipsize(null);
                isElementExpanded = true;
            }
            else {
                animation.cancel();
                animation = ObjectAnimator.ofInt(
                        tvText,
                        "maxLines",
                        5);
                animation.setDuration(200);
                animation.start();
                btnToggleSize.setBackgroundResource(R.drawable.expand_more_24px);
                tvText.setEllipsize(TextUtils.TruncateAt.END);
                isElementExpanded = false;
            }
        }
    }

    public static class ViewHolderPublication extends RecyclerView.ViewHolder {
        private TextView tvTitle, tvAuthors, tvSize, tvYear;
        private ImageView ivLanguage, ivFormat;
        private Button btnToDocument;

        public ViewHolderPublication(View view) {
            super(view);
            // Define click listener for the ViewHolder's View
            tvTitle = view.findViewById(R.id.tv_publication_title);
            tvAuthors = view.findViewById(R.id.tv_authors);
            tvSize = view.findViewById(R.id.tv_size);
            tvYear = view.findViewById(R.id.tv_year);
            ivLanguage = view.findViewById(R.id.iv_language);
            ivFormat = view.findViewById(R.id.iv_format);
            btnToDocument = view.findViewById(R.id.btn_go_to_document);
        }

        public TextView getTvTitle() {
            return tvTitle;
        }
        public TextView getTvAuthors() {
            return tvAuthors;
        }
        public TextView getTvSize() {
            return tvSize;
        }
        public TextView getTvYear() {
            return tvYear;
        }
        public ImageView getIvLanguage() { return ivLanguage; }
        public ImageView getIvFormat() { return ivFormat; }
        public Button getBtnToDocument() { return btnToDocument; }
    }

    public SearchResultsListAdapter(ArrayList<ContentWrapper> contentList, Context context) {
        this.contentList = contentList;
        this.context = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view, which defines the UI of the list item
        if (viewType == ContentWrapper.CONTENT_TYPE_ARTICLE) {
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.fragment_newsticker_listelement, viewGroup, false);
            return new ViewHolderArticle(view);
        }
        else {
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.fragment_publication_listelement, viewGroup, false);
            return new ViewHolderPublication(view);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return contentList.get(position).getContentType();
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int position) {
        switch (viewHolder.getItemViewType()) {
            case ContentWrapper.CONTENT_TYPE_ARTICLE:
                final ViewHolderArticle vha = (ViewHolderArticle) viewHolder;
                final Article art = (Article) contentList.get(position).getContent();
                vha.getBtnToggleSize().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        vha.toggleTextSize();
                    }
                });
                vha.getTvTitle().setText(art.getTitle());
                vha.getTvText().setText(art.getText());
                String dateString = art.getDate().toString() + " ";
                vha.getTvDate().setText(dateString);
                ArrayList<WebLink> linkList = art.getWebLinks();
                if (linkList != null) {
                    for (int i = 0; i < linkList.size(); i++) {
                        String linkText;
                        if (linkList.get(i).getDisplayedText().equals("")) {
                            linkText = "mehr";
                        }
                        else {
                            linkText = linkList.get(i).getDisplayedText();
                        }
                        String text = "<a href='" + linkList.get(i).getLink() + "'>" + linkText + "</a>";
                        switch (i) {
                            case 0:
                                vha.getTvLink1().setMovementMethod(LinkMovementMethod.getInstance());
                                vha.getTvLink1().setText(Html.fromHtml(text));
                                vha.getTvLink1().setVisibility(View.VISIBLE);
                                break;
                            case 1:
                                vha.getTvLink2().setMovementMethod(LinkMovementMethod.getInstance());
                                vha.getTvLink2().setText(Html.fromHtml(text));
                                vha.getTvLink2().setVisibility(View.VISIBLE);
                                break;
                            case 2:
                                vha.getTvLink3().setMovementMethod(LinkMovementMethod.getInstance());
                                vha.getTvLink3().setText(Html.fromHtml(text));
                                vha.getTvLink3().setVisibility(View.VISIBLE);
                                break;
                            case 3:
                                vha.getTvLink4().setMovementMethod(LinkMovementMethod.getInstance());
                                vha.getTvLink4().setText(Html.fromHtml(text));
                                vha.getTvLink4().setVisibility(View.VISIBLE);
                                break;
                            case 4:
                                vha.getTvLink5().setMovementMethod(LinkMovementMethod.getInstance());
                                vha.getTvLink5().setText(Html.fromHtml(text));
                                vha.getTvLink5().setVisibility(View.VISIBLE);
                                break;
                        }
                    }
                }
                break;
            case ContentWrapper.CONTENT_TYPE_PUBLICATION:
                final ViewHolderPublication vhp = (ViewHolderPublication) viewHolder;
                final Publication pub = (Publication) contentList.get(position).getContent();
                vhp.getTvTitle().setText(pub.getTitle());
                vhp.getTvAuthors().setText(pub.getAuthors());
                vhp.getTvSize().setText(pub.getFilesize());
                String yearText = String.valueOf(pub.getYear());
                vhp.getTvYear().setText(yearText);
                switch (pub.getFormat()) {
                    case Pdf:
                        vhp.getIvFormat().setImageResource(R.drawable.pdf_128px);
                        break;
                    case OnlineOnly:
                        vhp.getIvFormat().setImageResource(R.drawable.html_128px);
                }
                switch (pub.getLanguage()) {
                    case German:
                        vhp.getIvLanguage().setImageResource(R.drawable.german_flag);
                        break;
                    case English:
                        vhp.getIvLanguage().setImageResource(R.drawable.british_flag);
                        break;
                }
                vhp.getBtnToDocument().setOnClickListener(new View.OnClickListener() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onClick(View v) {
                        try {
                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_VIEW);
                            intent.addCategory(Intent.CATEGORY_BROWSABLE);
                            intent.setData(Uri.parse(pub.getWeblink()));
                            context.startActivity(intent);
                        }
                        catch (Exception e) {
                            Log.e(TAG, e.getMessage());
                            Toast.makeText(context, "The link is broken", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return contentList.size();
    }

    public ArrayList<ContentWrapper> getContentList() {
        return contentList;
    }
}
