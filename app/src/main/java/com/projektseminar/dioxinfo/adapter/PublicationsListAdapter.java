package com.projektseminar.dioxinfo.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.projektseminar.dioxinfo.R;
import com.projektseminar.dioxinfo.contentmanagement.PublicationManager;
import com.projektseminar.dioxinfo.contentmanagement.data.Publication;

import java.util.ArrayList;

public class PublicationsListAdapter extends RecyclerView.Adapter<PublicationsListAdapter.ViewHolder> {

    private String TAG = "PublicationsListAdapter";

    private ArrayList<Publication> publicationArrayList;
    private Context context;

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle, tvAuthors, tvSize, tvYear;
        private ImageView ivLanguage, ivFormat;
        private Button btnToDocument;

        public ViewHolder(View view) {
            super(view);
            // Define click listener for the ViewHolder's View
            tvTitle = view.findViewById(R.id.tv_publication_title);
            tvAuthors = view.findViewById(R.id.tv_authors);
            tvSize = view.findViewById(R.id.tv_size);
            tvYear = view.findViewById(R.id.tv_year);
            ivLanguage = view.findViewById(R.id.iv_language);
            ivFormat = view.findViewById(R.id.iv_format);
            btnToDocument = view.findViewById(R.id.btn_go_to_document);
        }

        public TextView getTvTitle() {
            return tvTitle;
        }
        public TextView getTvAuthors() {
            return tvAuthors;
        }
        public TextView getTvSize() {
            return tvSize;
        }
        public TextView getTvYear() {
            return tvYear;
        }
        public ImageView getIvLanguage() { return ivLanguage; }
        public ImageView getIvFormat() { return ivFormat; }
        public Button getBtnToDocument() { return btnToDocument; }
    }

    public PublicationsListAdapter(Context context) {
        this.context = context;
        this.publicationArrayList = PublicationManager.getInstance().getPublicationList();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.fragment_publication_listelement, viewGroup, false);
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        // Get element from your dataset at this position and replace the
        // contents of the view with that element

        viewHolder.getTvTitle().setText(publicationArrayList.get(position).getTitle());
        viewHolder.getTvAuthors().setText(publicationArrayList.get(position).getAuthors());
        viewHolder.getTvSize().setText(publicationArrayList.get(position).getFilesize());
        String yearText = String.valueOf(publicationArrayList.get(position).getYear());
        viewHolder.getTvYear().setText(yearText);
        switch (publicationArrayList.get(position).getFormat()) {
            case Pdf:
                viewHolder.getIvFormat().setImageResource(R.drawable.pdf_128px);
                break;
            case OnlineOnly:
                viewHolder.getIvFormat().setImageResource(R.drawable.html_128px);
        }
        switch (publicationArrayList.get(position).getLanguage()) {
            case German:
                viewHolder.getIvLanguage().setImageResource(R.drawable.german_flag);
                break;
            case English:
                viewHolder.getIvLanguage().setImageResource(R.drawable.british_flag);
                break;
        }
        viewHolder.getBtnToDocument().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.addCategory(Intent.CATEGORY_BROWSABLE);
                    intent.setData(Uri.parse(publicationArrayList.get(position).getWeblink()));
                    context.startActivity(intent);
                }
                catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                    Toast.makeText(context, "The link is broken", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return publicationArrayList.size();
    }
}