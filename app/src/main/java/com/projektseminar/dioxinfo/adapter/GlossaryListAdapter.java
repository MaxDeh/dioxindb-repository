package com.projektseminar.dioxinfo.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.projektseminar.dioxinfo.R;
import com.projektseminar.dioxinfo.contentmanagement.data.Glossary;

import java.util.ArrayList;

public class GlossaryListAdapter extends RecyclerView.Adapter<GlossaryListAdapter.ViewHolder> {

    ArrayList<Glossary> glossary_entry_list = new ArrayList<Glossary>();

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle;
        private TextView tvText;
        private ImageButton btnToggleSize;
        private boolean isElementExpanded;
        private LinearLayout llBackground;

        public ViewHolder(View view) {
            super(view);
            // Define click listener for the ViewHolder's View
            tvTitle = view.findViewById(R.id.tv_title_glossary);
            tvText = view.findViewById(R.id.tv_text_glossary);
            btnToggleSize = view.findViewById(R.id.btn_toggle_size_glossary);
            llBackground = view.findViewById(R.id.ll_glossary);
            isElementExpanded = false;
        }

        public TextView getTvTitle() {
            return tvTitle;
        }

        public TextView getTvText() {
            return tvText;
        }

        public ImageButton getBtnToggleSize() { return btnToggleSize; }

        public void toggleTextSize() {
            if (!isElementExpanded) {
                btnToggleSize.setBackgroundResource(R.drawable.expand_less_24px);
                llBackground.setBackgroundResource(R.drawable.grey_title_top_rounded_corners);
                tvText.setPadding(30, 10, 30, 10);
                tvText.setMaxLines(9999);
                isElementExpanded = true;
            }
            else {
                btnToggleSize.setBackgroundResource(R.drawable.expand_more_24px);
                llBackground.setBackgroundResource(R.drawable.grey_title_rounded_4_corners);
                tvText.setPadding(0, 0, 0, 0);
                tvText.setMaxLines(0);
                isElementExpanded = false;
            }
        }
    }

    public GlossaryListAdapter(ArrayList<Glossary> generated_glossary_list) {
        glossary_entry_list=generated_glossary_list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.fragment_glossary_listelement, viewGroup, false);

        return new GlossaryListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        viewHolder.getBtnToggleSize().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewHolder.toggleTextSize();
            }
        });
        // Get element from your dataset at this position and replace the
        // contents of the view with that element

        viewHolder.getTvTitle().setText(glossary_entry_list.get(position).getWord());
        //TODO remove following comment to set text to actual article text
        viewHolder.getTvText().setText(glossary_entry_list.get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        return glossary_entry_list.size();
    }

    public ArrayList<Glossary> getGlossaryList() {
        return glossary_entry_list;
    }

}
