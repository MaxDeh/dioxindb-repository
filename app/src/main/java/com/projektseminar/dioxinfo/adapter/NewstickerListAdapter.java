package com.projektseminar.dioxinfo.adapter;

import android.animation.ObjectAnimator;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import com.projektseminar.dioxinfo.R;
import com.projektseminar.dioxinfo.contentmanagement.ArticleManager;
import com.projektseminar.dioxinfo.contentmanagement.data.Article;
import com.projektseminar.dioxinfo.contentmanagement.data.WebLink;

import java.util.ArrayList;

public class NewstickerListAdapter extends RecyclerView.Adapter<NewstickerListAdapter.ViewHolder> {

    ArrayList<Article> articleArrayList;

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle, tvText, tvDate, tvLink1, tvLink2, tvLink3, tvLink4, tvLink5;
        private ImageButton btnToggleSize;
        private boolean isElementExpanded;
        private ObjectAnimator animation;

        public ViewHolder(View view) {
            super(view);
            // Define click listener for the ViewHolder's View
            tvTitle = view.findViewById(R.id.tv_title);
            tvText = view.findViewById(R.id.tv_text);
            tvDate = view.findViewById(R.id.tv_date);
            tvLink1 = view.findViewById(R.id.tv_link1);
            tvLink2 = view.findViewById(R.id.tv_link2);
            tvLink3 = view.findViewById(R.id.tv_link3);
            tvLink4 = view.findViewById(R.id.tv_link4);
            tvLink5 = view.findViewById(R.id.tv_link5);
            btnToggleSize = view.findViewById(R.id.btn_toggle_size);
            animation = new ObjectAnimator();
            isElementExpanded = false;
        }

        public TextView getTvDate() { return tvDate; }
        public TextView getTvTitle() {
            return tvTitle;
        }
        public TextView getTvText() {
            return tvText;
        }
        public TextView getTvLink1() { return tvLink1; }
        public TextView getTvLink2() { return tvLink2; }
        public TextView getTvLink3() { return tvLink3; }
        public TextView getTvLink4() { return tvLink4; }
        public TextView getTvLink5() { return tvLink5; }

        public ImageButton getBtnToggleSize() { return btnToggleSize; }

        public void toggleTextSize() {
            if (!isElementExpanded) {
                animation.cancel();
                animation = ObjectAnimator.ofInt(
                        tvText,
                        "maxLines",
                        150);
                animation.setDuration(1000);
                animation.start();
                btnToggleSize.setBackgroundResource(R.drawable.expand_less_24px);
                tvText.setEllipsize(null);
                isElementExpanded = true;
            }
            else {
                animation.cancel();
                animation = ObjectAnimator.ofInt(
                        tvText,
                        "maxLines",
                        5);
                animation.setDuration(200);
                animation.start();
                btnToggleSize.setBackgroundResource(R.drawable.expand_more_24px);
                tvText.setEllipsize(TextUtils.TruncateAt.END);
                isElementExpanded = false;
            }
        }
    }

    public NewstickerListAdapter() {
        this.articleArrayList = ArticleManager.getInstance().getArticleList();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view, which defines the UI of the list item
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.fragment_newsticker_listelement, viewGroup, false);
        return new ViewHolder(view);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        viewHolder.getBtnToggleSize().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewHolder.toggleTextSize();
            }
        });
        viewHolder.getTvTitle().setText(articleArrayList.get(position).getTitle());
        viewHolder.getTvText().setText(articleArrayList.get(position).getText());
        String dateString = articleArrayList.get(position).getDate().toString() + " ";
        viewHolder.getTvDate().setText(dateString);
        ArrayList<WebLink> linkList = articleArrayList.get(position).getWebLinks();
        if (linkList != null) {
            for (int i = 0; i < linkList.size(); i++) {
                String linkText;
                if (linkList.get(i).getDisplayedText().equals("")) {
                    linkText = "mehr";
                }
                else {
                    linkText = linkList.get(i).getDisplayedText();
                }
                String text = "<a href='" + linkList.get(i).getLink() + "'>" + linkText + "</a>";
                switch (i) {
                    case 0:
                        viewHolder.getTvLink1().setMovementMethod(LinkMovementMethod.getInstance());
                        viewHolder.getTvLink1().setText(Html.fromHtml(text));
                        viewHolder.getTvLink1().setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        viewHolder.getTvLink2().setMovementMethod(LinkMovementMethod.getInstance());
                        viewHolder.getTvLink2().setText(Html.fromHtml(text));
                        viewHolder.getTvLink2().setVisibility(View.VISIBLE);
                        break;
                    case 2:
                        viewHolder.getTvLink3().setMovementMethod(LinkMovementMethod.getInstance());
                        viewHolder.getTvLink3().setText(Html.fromHtml(text));
                        viewHolder.getTvLink3().setVisibility(View.VISIBLE);
                        break;
                    case 3:
                        viewHolder.getTvLink4().setMovementMethod(LinkMovementMethod.getInstance());
                        viewHolder.getTvLink4().setText(Html.fromHtml(text));
                        viewHolder.getTvLink4().setVisibility(View.VISIBLE);
                        break;
                    case 4:
                        viewHolder.getTvLink5().setMovementMethod(LinkMovementMethod.getInstance());
                        viewHolder.getTvLink5().setText(Html.fromHtml(text));
                        viewHolder.getTvLink5().setVisibility(View.VISIBLE);
                        break;
                }
            }
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return articleArrayList.size();
    }
}
