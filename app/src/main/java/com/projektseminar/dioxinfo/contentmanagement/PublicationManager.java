package com.projektseminar.dioxinfo.contentmanagement;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.projektseminar.dioxinfo.contentmanagement.data.Publication;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

// Singleton class to handle all accessing of the list of publications
// Always access from outside classes via getInstance.getPublicationList()
// Make sure the setting of the publicationList happens before attempts to access it!

public class PublicationManager {

    private static final String TAG = "PublicationManager";
    private static final String FILENAME = "publicationList.ser";
    private static PublicationManager publicationManager;
    private static ArrayList<Publication> publicationList = new ArrayList<>();

    public void setPublicationList(ArrayList<Publication> publicationList) {
        PublicationManager.publicationList = publicationList;
        sortPublicationList();
    }

    public ArrayList<Publication> getPublicationList() { return publicationList; }

    private PublicationManager() { }

    public static PublicationManager getInstance(){
        if (PublicationManager.publicationManager == null) {
            PublicationManager.publicationManager = new PublicationManager();
        }
        return PublicationManager.publicationManager;
    }

    public void serializePublicationList(Activity context){
        try{
            FileOutputStream fos = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
            ObjectOutputStream out = new ObjectOutputStream(fos);
            out.writeObject(getPublicationList());
            out.close();
            fos.close();
        }
        catch(Exception e){
            Log.v(TAG, "Error while write SER File: " + e.toString());
        }
    }

    public void deserializePublicationList(Activity context){
        try{
            FileInputStream fis = context.openFileInput(FILENAME);
            ObjectInputStream ois = new ObjectInputStream(fis);
            setPublicationList( (ArrayList<Publication>) ois.readObject() );
            ois.close();
            fis.close();
        }
        catch(FileNotFoundException e){
            serializePublicationList(context);
        }
        catch(Exception e){
            Log.v(TAG, e.toString());
        }
        finally {
            if (publicationList == null) {
                publicationList = new ArrayList<>();
            }
        }
    }

    //bubble sort, pos 0 should be newest
    private void sortPublicationList() {
        boolean sorted = false;
        //years
        while (!sorted) {
            sorted = true;
            for (int i = publicationList.size() - 1; i > 0; i--) {
                //is the year of i larger than the year of i-1?
                if (publicationList.get(i).getYear() > publicationList.get(i - 1).getYear()) {
                    Publication pub = publicationList.get(i - 1);
                    publicationList.set(i - 1, publicationList.get(i));
                    publicationList.set(i, pub);
                    sorted = false;
                }
            }
        }
    }
}
