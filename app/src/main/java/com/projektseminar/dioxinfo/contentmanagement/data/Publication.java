package com.projektseminar.dioxinfo.contentmanagement.data;

import java.io.Serializable;

public class Publication extends Content  implements Serializable {

    private String title;
    private String authors;
    private int year;
    private String filesize;
    private Format format;
    private Language language;
    private String weblink;


    public Publication(String databaseId, String title, int year, String filesize, String authors, Format format, Language language, String weblink){
        setDatabaseId(databaseId);
        this.title = title;
        this.authors = authors;
        this.year = year;
        this.filesize = filesize;
        this.format = format;
        this.language = language;
        this.weblink = weblink;
    }

    @Override
    public String getTitle(){ return title; }

    public String getAuthors() {
        return authors;
    }

    public int getYear() { return year; }

    public String getFilesize() { return filesize; }

    public Format getFormat(){
        return format;
    }

    public Language getLanguage() {
        return language;
    }

    public String getWeblink() {
        return weblink;
    }


    public enum Format{
        Pdf,
        OnlineOnly
    }

    public enum Language{
        German,
        English
    }

}
