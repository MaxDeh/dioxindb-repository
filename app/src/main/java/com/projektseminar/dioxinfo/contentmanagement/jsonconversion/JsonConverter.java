package com.projektseminar.dioxinfo.contentmanagement.jsonconversion;

import android.util.Log;

import com.projektseminar.dioxinfo.contentmanagement.data.Article;
import com.projektseminar.dioxinfo.contentmanagement.data.Publication;
import com.projektseminar.dioxinfo.contentmanagement.data.WebLink;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class JsonConverter {

    private static final String[] articleKeys = {"_id", "date", "titles", "texts", "weblinks"};
    private static final String[] publicationKeys = {"_id", "title", "year", "filesize", "authors", "format", "language", "weblink"};
    private static final String[] weblinkKeys = {"link", "displayedtexts"};

    private JsonConverter(){
    }

    public static Article convertJsonToArticle(JSONObject jsonObject) throws JSONException {
        for(String key : articleKeys){
            if (!jsonObject.has(key)){
                Log.i("JsonConverter", "Fehler bei Umwandlung des Keys: " + key);
                return null;
            }
        }
        ArrayList<String> titleList = convertJsonArrayToStringList(jsonObject.getJSONArray(articleKeys[2]), "title");
        ArrayList<String> textList = convertJsonArrayToStringList(jsonObject.getJSONArray(articleKeys[3]), "text");
        ArrayList<WebLink> weblinkList = convertJsonArrayToWeblinkList(jsonObject.getJSONArray(articleKeys[4]));

        return new Article(jsonObject.getString(articleKeys[0]),
                            jsonObject.getString(articleKeys[1]),
                            titleList,
                            textList,
                            weblinkList);
    }

    public static Publication convertJsonToPublication(JSONObject jsonObject) throws JSONException{
        for(String key : publicationKeys){
            if (!jsonObject.has(key)){
                Log.i("JsonConverter", "Fehler bei Umwandlung des Keys: " + key);
                return null;
            }
        }

        return new Publication(jsonObject.getString(publicationKeys[0]),
                                jsonObject.getString(publicationKeys[1]),
                                jsonObject.getInt(publicationKeys[2]),
                                jsonObject.getString(publicationKeys[3]),
                                jsonObject.getString(publicationKeys[4]),
                                Publication.Format.values()[jsonObject.getInt(publicationKeys[5])],
                                Publication.Language.values()[jsonObject.getInt(publicationKeys[6])],
                                jsonObject.getString(publicationKeys[7]));
    }

    public static Article convertStringToArticle(String s) throws JSONException{
        JSONObject jsonObject = new JSONObject(s);

        return convertJsonToArticle(jsonObject);
    }

    public static Publication convertStringToPublication(String s) throws JSONException{
        JSONObject jsonObject = new JSONObject(s);

        return convertJsonToPublication(jsonObject);
    }

    public static ArrayList<WebLink> convertJsonArrayToWeblinkList(JSONArray jsonArray) throws JSONException{
        ArrayList<WebLink> weblinkList = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            for(String key : weblinkKeys){
                if (!jsonArray.getJSONObject(i).has(key)){
                    Log.i("JsonConverter", "Fehler bei Umwandlung des Keys: " + key);
                    return null;
                }
            }
            ArrayList<String> displayedTextList = convertJsonArrayToStringList(jsonArray.getJSONObject(i).getJSONArray(weblinkKeys[1]), "displayedtext");
            weblinkList.add(new WebLink(jsonArray.getJSONObject(i).getString(weblinkKeys[0]), displayedTextList));
        }

        return weblinkList;
    }

    public static ArrayList<String> convertJsonArrayToStringList(JSONArray jsonArray, String keyname) throws JSONException{
        ArrayList<String> stringList = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            stringList.add(jsonArray.getJSONObject(i).getString(keyname));
        }

        return stringList;
    }

}
