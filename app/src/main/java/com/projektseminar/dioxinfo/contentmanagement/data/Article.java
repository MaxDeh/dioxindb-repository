package com.projektseminar.dioxinfo.contentmanagement.data;

import com.projektseminar.dioxinfo.screens.managers.UserSettingsManager;

import java.io.Serializable;
import java.util.ArrayList;

public class Article extends Content  implements Serializable {

    private ArrayList<String> titleList;
    private ArrayList<String> textList;
    private ArrayList<WebLink> webLinks = new ArrayList<>();
    private Date date;

    public Article(String databaseId, String date, ArrayList<String> titleList, ArrayList<String> textList, ArrayList<WebLink> webLinks) {
        setDatabaseId(databaseId);
        this.date = Date.parseDate(date);
        this.titleList = titleList;
        this.textList = textList;
        this.webLinks = webLinks;
    }

    @Override
    public String getTitle(){
        int languageId = UserSettingsManager.getInstance().getUserLanguage().ordinal();
        return titleList.get(languageId);
    }

    public String getText(){
        int languageId = UserSettingsManager.getInstance().getUserLanguage().ordinal();
        return textList.get(languageId);
    }

    public Date getDate() { return date; }

    public ArrayList<WebLink> getWebLinks(){
        return webLinks;
    }

    public float getSearchScoreByText(String input) {
        String[] textWords = getText().split(" ");
        input = input.toLowerCase();
        int hits = 0;
        for (String word : textWords) {
            word = word.toLowerCase();
            if (word.contains(input)){
                hits++;
            }
        }

        return  (float) hits / (float) textWords.length;
    }
}
