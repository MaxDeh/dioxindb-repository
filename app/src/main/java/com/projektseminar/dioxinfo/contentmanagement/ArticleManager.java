package com.projektseminar.dioxinfo.contentmanagement;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.projektseminar.dioxinfo.contentmanagement.data.Article;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

// Singleton class to handle all accessing of the list of articles
// Always access from outside classes via getInstance.getArticleList()
// Make sure the setting of the articleList happens before attempts to access it!

public class ArticleManager {

    private static final String TAG = "ArticleManager";
    private static final String FILENAME = "articleList.ser";
    private static ArticleManager articleManager;
    private ArrayList<Article> articleList;

    public void setArticleList(ArrayList<Article> articleList) {
        articleManager.articleList = articleList;
        sortArticleList();
    }

    public ArrayList<Article> getArticleList() {
        return ArticleManager.articleManager.articleList;
    }

    private ArticleManager() { }

    public static ArticleManager getInstance(){
        if (ArticleManager.articleManager == null) {
            ArticleManager.articleManager = new ArticleManager();
        }
        return ArticleManager.articleManager;
    }

    public void serializeArticleList(Activity context){
        try{
            FileOutputStream fos = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
            ObjectOutputStream out = new ObjectOutputStream(fos);
            out.writeObject(getArticleList());
            out.close();
            fos.close();
        }
        catch(Exception e){
            Log.v(TAG, "Error while write SER File: " + e.toString());
        }
    }

    public void deserializeArticleList(Activity context){
        try{
            FileInputStream fis = context.openFileInput(FILENAME);
            ObjectInputStream ois = new ObjectInputStream(fis);
            setArticleList( (ArrayList<Article>) ois.readObject() );
            ois.close();
            fis.close();
        }
        catch(FileNotFoundException e){
            serializeArticleList(context);
        }
        catch(Exception e){
            Log.v(TAG, e.toString());
        }
        finally {
            if (articleList == null) {
                articleList = new ArrayList<>();
            }
        }
    }

    //bubble sort, pos 0 should be newest
    private void sortArticleList() {
        boolean sorted = false;
        //years
        while (!sorted) {
            sorted = true;
            for (int i = articleList.size() - 1; i > 0; i--) {
                //is the year of i larger than the year of i-1?
                if (articleList.get(i).getDate().getYear() > articleList.get(i - 1).getDate().getYear()) {
                    Article art = articleList.get(i - 1);
                    articleList.set(i - 1, articleList.get(i));
                    articleList.set(i, art);
                    sorted = false;
                }
                //if the years are equal, is the month of i larger than the month of i-1?
                else if (articleList.get(i).getDate().getYear().equals(articleList.get(i - 1).getDate().getYear())) {
                    if (articleList.get(i).getDate().getMonth() > articleList.get(i - 1).getDate().getMonth()) {
                        Article art = articleList.get(i - 1);
                        articleList.set(i - 1, articleList.get(i));
                        articleList.set(i, art);
                        sorted = false;
                    }
                }
                //if the years and the months are equal, is the month of i larger than the month of i-1?
                else if (articleList.get(i).getDate().getYear().equals(articleList.get(i - 1).getDate().getYear()) && articleList.get(i).getDate().getMonth().equals(articleList.get(i - 1).getDate().getMonth())) {
                    if (articleList.get(i).getDate().getDay() > articleList.get(i - 1).getDate().getDay()) {
                        Article art = articleList.get(i - 1);
                        articleList.set(i - 1, articleList.get(i));
                        articleList.set(i, art);
                        sorted = false;
                    }
                }
            }
        }
    }
}
