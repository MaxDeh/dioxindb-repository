package com.projektseminar.dioxinfo.contentmanagement;

import android.util.Log;

import com.projektseminar.dioxinfo.contentmanagement.data.Article;
import com.projektseminar.dioxinfo.contentmanagement.data.Content;
import com.projektseminar.dioxinfo.contentmanagement.data.ContentWrapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchManager {

    public static final String TAG = "SearchManager";

    private static SearchManager sm;

    private SearchManager(){ }

    public static SearchManager getInstance() {
        if (sm == null) {
            sm = new SearchManager();
        }
        return sm;
    }

    private <T extends Content> ArrayList<T> generateSearchResultsOf(List<T> baseList, String input, boolean useTitle){
        HashMap<T, Float> unsortedResults = new HashMap();
        for ( T content : baseList ) {
            float score;
            if (useTitle) score = content.getSearchScoreByTitle(input);
            else {
                Article article = (Article) content;
                score = article.getSearchScoreByText(input);
            }

            if (score > 0f){
                unsortedResults.put(content, score);
            }
        }

        List<Map.Entry<T, Float>> results = new ArrayList<>(
                unsortedResults.entrySet()
        );

        Collections.sort(
                results,
                new Comparator<Map.Entry<T, Float>>() {
                    @Override
                    public int compare(Map.Entry<T, Float> a, Map.Entry<T, Float> b) {
                        return Float.compare(b.getValue(), a.getValue());
                    }
                }
        );

        ArrayList<T> finalResults = new ArrayList<>();

        for (Map.Entry<T, Float> result : results){
            Log.v(TAG, result.getKey().getTitle());
            finalResults.add(result.getKey());
        }

        return finalResults;
    }

    public ArrayList<ContentWrapper> generateAllSearchResults(String input){
        ArrayList<Content> everything = new ArrayList<>();
        everything.addAll(ArticleManager.getInstance().getArticleList());
        everything.addAll(PublicationManager.getInstance().getPublicationList());

        ArrayList<Content> results = generateSearchResultsOf(everything, input, true);

        ArrayList<Article> articles = new ArrayList<>();
        ArrayList<Integer> indices = new ArrayList<>();

        for (Content result : results){
            if (result instanceof Article){
                articles.add((Article) result);
                indices.add(results.indexOf(result));
            }
        }

        articles = (ArrayList<Article>) generateSearchResultsOf(articles, input, false);

        for (int i = 0; i < articles.size(); i++){
            results.set(indices.get(i), articles.get(i));
        }

        ArrayList<ContentWrapper> contentWrapperArrayList = new ArrayList<>();

        for (Content c : results) {
            if (c instanceof Article) contentWrapperArrayList.add(new ContentWrapper(c, ContentWrapper.CONTENT_TYPE_ARTICLE));
            else contentWrapperArrayList.add(new ContentWrapper(c, ContentWrapper.CONTENT_TYPE_PUBLICATION));
        }

        return contentWrapperArrayList;
    }
}
