package com.projektseminar.dioxinfo.contentmanagement.data;

public class Glossary {

    private String word;
    private String description;

    public Glossary(String word, String description) {
        this.word = word;
        this.description = description;
    }

    public String getWord() { return word; }

    public String getDescription() {
        return description;
    }
}
