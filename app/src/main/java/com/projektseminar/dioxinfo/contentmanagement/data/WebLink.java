package com.projektseminar.dioxinfo.contentmanagement.data;

import com.projektseminar.dioxinfo.screens.managers.UserSettingsManager;

import java.io.Serializable;
import java.util.ArrayList;

public class WebLink implements Serializable {

    private String link;
    private ArrayList<String> displayedText;

    public WebLink(String link, ArrayList<String> displayedText){
        this.link = link;
        this.displayedText = displayedText;
    }

    public String getDisplayedText() {
        int languageId = UserSettingsManager.getInstance().getUserLanguage().ordinal();
        return displayedText.get(languageId);
    }

    public String getLink() {
        return link;
    }
}
