package com.projektseminar.dioxinfo.contentmanagement.data;

import java.io.Serializable;

public abstract class Content  implements Serializable {

    private String databaseId = "";

    public String getDatabaseId(){
        return databaseId;
    }

    public void setDatabaseId(String databaseId){
        this.databaseId = databaseId;
    }

    public abstract String getTitle();


    //single word
    public float getSearchScoreByTitle(String input){
        String[] titleWords = getTitle().split(" ");
        int hits = 0;
        for (String word : titleWords) {
            word = word.toLowerCase();
            if (word.contains(input)){
                hits++;
            }
        }

        return  (float) hits / (float) titleWords.length;
    }

}
