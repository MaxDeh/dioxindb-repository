package com.projektseminar.dioxinfo.contentmanagement.data;

import android.util.Log;

import java.io.Serializable;


public class Date implements Serializable {

    private static String TAG = "DateClass";

    private Integer day;
    private Integer month;
    private Integer year;


    public Date(int d, int m, int y){
        day = d;
        month = m;
        year = y;
    }

    //Parse Format dd.mm.yyyy to date
    public static Date parseDate(String dateText) {
        try {
            if(dateText.length() != 10){
                throw new IndexOutOfBoundsException("Length of string is not 10!");
            }
            String dayText = dateText.substring(0, 2);
            String monthText = dateText.substring(3, 5);
            String yearText = dateText.substring(6, 10);
            int day = Integer.parseInt(dayText);
            int month = Integer.parseInt(monthText);
            int year = Integer.parseInt(yearText);

            return new Date(day, month, year);
        }
        catch(NumberFormatException e) {
            Log.v(TAG, "Error while parsing to date by string.");
        }
        catch(Exception e) {
            Log.v(TAG, e.toString());
        }
        return null;

    }

    public String toString() {
        return day.toString() + "." + month.toString() + "." + year.toString();
    }

    public Integer getDay() {
        return day;
    }

    public Integer getMonth() {
        return month;
    }

    public Integer getYear() {
        return year;
    }
}
