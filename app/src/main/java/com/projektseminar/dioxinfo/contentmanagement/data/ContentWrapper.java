package com.projektseminar.dioxinfo.contentmanagement.data;

public class ContentWrapper {

    public static final int CONTENT_TYPE_ARTICLE = 0;
    public static final int CONTENT_TYPE_PUBLICATION = 1;

    private Content content;
    private int contentType;

    public ContentWrapper(Content content, int contentType) {
        this.content = content;
        this.contentType = contentType;
    }

    public int getContentType() {
        return contentType;
    }

    public Content getContent() {
        return content;
    }
}
