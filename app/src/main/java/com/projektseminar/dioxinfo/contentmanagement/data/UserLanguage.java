package com.projektseminar.dioxinfo.contentmanagement.data;

public enum UserLanguage {
    de,
    en;
}
