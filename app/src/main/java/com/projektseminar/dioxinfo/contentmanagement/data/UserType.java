package com.projektseminar.dioxinfo.contentmanagement.data;

public enum UserType {
    CITIZEN,
    PRESS,
    SCIENTIST;
}
