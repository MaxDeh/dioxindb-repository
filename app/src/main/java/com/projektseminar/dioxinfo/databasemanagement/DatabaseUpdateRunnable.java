package com.projektseminar.dioxinfo.databasemanagement;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

public class DatabaseUpdateRunnable extends AsyncTask<String, Void, Integer> {

    private static String TAG = "DatabaseUpdateRunnable";
    private static int timeout = 10000;
    private String ip;
    private String id;
    private ArrayList<DatabaseResponse> responseList;
    private DatabaseManager manager;

    public DatabaseUpdateRunnable(String ip, String id, ArrayList<DatabaseResponse> responseList, DatabaseManager manager){
        this.ip = ip;
        this.id = id;
        this.responseList = responseList;
        this.responseList.clear();
        this.manager = manager;
    }

    public ArrayList<DatabaseResponse> getResponseList() { return responseList; }

    //set a GET request to the controll document, which stores all Id's
    //Than set a GET request for each gathered Id
    @Override
    protected Integer doInBackground(String... strings) {
        try{
            DatabaseResponse response = doGetRequest(this.id);
            if (response.getResponseCode() == HttpURLConnection.HTTP_OK){
                JSONObject controlDocJson = new JSONObject(response.getResponseText());
                ArrayList<String> articleIdList = getIdArray(controlDocJson, "article_id_array", "article_id");
                ArrayList<String> publicationIdList = getIdArray(controlDocJson, "publication_id_array", "publication_id");
                for(String id : articleIdList){
                    DatabaseResponse articleResponse = doGetRequest(id);
                    if (articleResponse.getResponseCode() == HttpURLConnection.HTTP_OK){
                        articleResponse.setType(DatabaseResponse.Type.ARTICLE);
                        responseList.add(articleResponse);
                    }
                }
                for(String id : publicationIdList){
                    DatabaseResponse publicationResponse = doGetRequest(id);
                    if (publicationResponse.getResponseCode() == HttpURLConnection.HTTP_OK){
                        publicationResponse.setType(DatabaseResponse.Type.PUBLICATION);
                        responseList.add(publicationResponse);
                    }
                }

                return HttpsURLConnection.HTTP_OK;
            }
        }
        catch(Exception e){
            Log.v("TAG", e.getMessage());
        }
        return -1;
    }

    @Override
    protected void onPostExecute(Integer code) {
        manager.handleAfterResponse(code);
    }

    private ArrayList<String> getIdArray(JSONObject controlDocJson, String arrayName, String idFieldName) throws JSONException{
        ArrayList<String> idList = new ArrayList<>();
        if (controlDocJson.has(arrayName)){
            JSONArray articleArray = controlDocJson.getJSONArray(arrayName);
            for (int i = 0; i < articleArray.length(); i++) {
                if (articleArray.getJSONObject(i).has(idFieldName)){
                    idList.add(articleArray.getJSONObject(i).getString(idFieldName));
                }
            }
        }
        return idList;
    }

    private DatabaseResponse doGetRequest(String id){
        DatabaseResponse response = new DatabaseResponse();
        try{
            URL url = new URL("http://" + ip + ":5984/dioxindb/" + id);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setConnectTimeout(timeout);
            response.setResponseCode(con.getResponseCode());
            if (response.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer responseBuffer = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    responseBuffer.append(inputLine);
                }
                in.close();
                response.setResponseText(responseBuffer.toString());
                Log.v(TAG, "GET Request succeed for ID: '" + id + "'" + " ResponseCode: " + "" + response.getResponseCode() + " ResponseText: " + response.getResponseText());
            }
            else {
                response.setResponseText("Error! ResponseCode: " + response.getResponseCode() + " ResponseText: " + response.getResponseText());
            }
        }
        catch(Exception e){
            response.setResponseText(e.getMessage());
        }
        return response;
    }
}
