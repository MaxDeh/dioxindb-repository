package com.projektseminar.dioxinfo.databasemanagement;

public class DatabaseResponse {

    private int responseCode = -1;
    private  String responseText = "";
    private Type type = Type.NONE;

    public enum Type{
        NONE,
        ARTICLE,
        PUBLICATION
    }

    public DatabaseResponse() {}

    public DatabaseResponse(int code, String text){
        responseCode = code;
        responseText = text;
    }

    public int getResponseCode() { return responseCode; }

    public String getResponseText() { return  responseText; }

    public Type getType() { return type; }

    public void setResponseCode(int code) { responseCode = code; }

    public void setResponseText(String text) { responseText = text; }

    public void setType(Type type) { this.type = type; }
}
