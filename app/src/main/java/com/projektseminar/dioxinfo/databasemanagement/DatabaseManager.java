package com.projektseminar.dioxinfo.databasemanagement;

import android.util.Log;

import com.projektseminar.dioxinfo.contentmanagement.ArticleManager;
import com.projektseminar.dioxinfo.contentmanagement.PublicationManager;
import com.projektseminar.dioxinfo.contentmanagement.data.Article;
import com.projektseminar.dioxinfo.contentmanagement.data.Publication;
import com.projektseminar.dioxinfo.contentmanagement.jsonconversion.JsonConverter;
import com.projektseminar.dioxinfo.screens.activities.ActivityMain;
import com.projektseminar.dioxinfo.screens.managers.CustomFragmentManager;

import java.net.HttpURLConnection;
import java.util.ArrayList;

public class DatabaseManager {

    private static String TAG = "DatabaseManager";
    private static String ip = "178.24.62.19";
    private final static String controlDocId = "07f987fb58028afeaa2f5af270006ca2";

    private static ArrayList<DatabaseResponse> responseList = new ArrayList<>();

    private ActivityMain activity;

    private static DatabaseUpdateRunnable thread;

    public DatabaseManager(ActivityMain activity){
        this.activity = activity;
        thread = new DatabaseUpdateRunnable(ip, controlDocId, responseList, this);
    }

    public void update(){
        thread.execute("");
    }

    //Convert all response strings to objects (articles, publications)
    //Override old lists
    public void handleAfterResponse(Integer code) {
        switch (code){
            case HttpURLConnection.HTTP_OK:
                try{
                    ArrayList<Article> articleList = new ArrayList<>();
                    ArrayList<Publication> publicationList = new ArrayList<>();
                    for(DatabaseResponse response : responseList){
                        if (response.getResponseCode() == HttpURLConnection.HTTP_OK){
                            if (response.getType() == DatabaseResponse.Type.ARTICLE){
                                Article article;
                                try{
                                    if ( (article = JsonConverter.convertStringToArticle(response.getResponseText())) != null){
                                        articleList.add(article);
                                    }
                                }
                                catch(Exception e){
                                    Log.v(TAG, "Error while converting article json: " + e.toString());
                                }
                            }
                            else if (response.getType() == DatabaseResponse.Type.PUBLICATION){
                                Publication publication;
                                try{
                                    if ( (publication = JsonConverter.convertStringToPublication(response.getResponseText())) != null){
                                        publicationList.add(publication);
                                    }
                                }
                                catch(Exception e){
                                    Log.v(TAG, "Error while converting publication json: " + e.toString());
                                }
                            }
                        }
                    }
                    ArticleManager.getInstance().setArticleList(articleList);
                    ArticleManager.getInstance().serializeArticleList(activity);
                    PublicationManager.getInstance().setPublicationList(publicationList);
                    PublicationManager.getInstance().serializePublicationList(activity);
                    CustomFragmentManager.getInstance().reloadNewsFragment();
                }
                catch (Exception e){
                    Log.v(TAG, e.getMessage() + "  " + e.toString());
                }
                break;
            case HttpURLConnection.HTTP_CLIENT_TIMEOUT:
                Log.v(TAG, "Error while updating. 408 ClientTimeout");
                break;
            case HttpURLConnection.HTTP_NOT_FOUND:
                Log.v(TAG, "Error while updating. 404 NotFound");
                break;
            default:
                Log.v(TAG, "Error while updating. Code: " + code.toString());
                break;
        }
    }
}
